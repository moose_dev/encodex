# En-Codex

A command line tool and library for several codes, ciphers and hashes. See features section for a
comprehensive list of supported features. For information about how to use this crate please use
`cargo doc --all-features` from within the project folder and read the generated documentation.

---

I develop this library/cli-tool mostly for lectures at university and understanding how all the
codes, ciphers and digest-algorithms work internally. It is not intended for use in production code.

If you still decide to use this library in production code or to play around with it, every
constructive feedback is appreciated.

---

---

## Features

The following list is a complete overview of all implemented features. Unchecked features are not
yet implemented.

The names of the `highlighted` features in this list match the names of features in the `Cargo.toml`
file. The list below the names is the functionality that will be enabled by the feature.

---

### Ciphers

#### Caesar

* [x] Encrypting
* [x] Decrypting
* [x] Test vectors

#### Vigenère

* [x] Encrypting
* [x] Decrypting
* [x] Test vectors

---

### Codes

#### _RFC 4648 - The Base16, Base32, and Base64 Data Encodings_

- [x] `base64`
  * [x] Encoding
  * [x] Decoding
  * [x] Test vectors (_from **RFC 4648** - 10_)
- [x] `base64url` en- and decoding
  * [x] Encoding
  * [x] Decoding
  * [x] Test vectors
- [x] `base32`
  * [x] Encoding
  * [x] Decoding
  * [x] Choose between upper- and lowercase
  * [x] Test vectors (_from **RFC 4648** - 10_)
- [x] `base32hex`
  * [x] Encoding
  * [x] Decoding
  * [x] Choose between upper- and lowercase
  * [x] Test vectors (_from **RFC 4648** - 10_)
- [x] `base16` / `hex`
  * [x] Encoding
  * [x] Decoding
  * [x] Choose between upper- and lowercase
  * [x] Test vectors (_from **RFC 4648** - 10_)
- [ ] Option for adding line feed after a certain amount of characters (_**RFC 4648** - 3.1_)
- [ ] Option for conditionally not adding padding bytes? Or different padding bytes?
      (_**RFC 4648** - 3.2_)
- [ ] Option for ignoring non-alphabet characters in base encoded data? (_**RFC 4648** - 3.3_)
- [ ] Option to change used alphabet for specific uses (_**RFC 4648** - 3.4_)

---

### Cryptanalysis

- [ ] `cryptanalysis`
  * [ ] Caesar
  * [ ] Vigenère

---

### Digests

#### SHA-2

- [ ] SHA-224
- [ ] SHA-256
- [ ] SHA-384
- [ ] SHA-512
- [ ] SHA-512/224
- [ ] SHA-512/256

---

### UI elements

- [x] `ui`
  * [x] Perform non-interactive encode,decode,encrypt,decrypt or hash operation
  * [ ] Use program interactively
  * [ ] Chain multiple cryptographic atoms

---

###### License

This program is released under the _GNU General Public License_. See _[COPYING](./COPYING)_ for
additional information about the license.

###### Contact

For any other question contact me at
<i>[fabian.moos@moosegamesdev.org](mailto:fabian.moos@moosegamesdev.org)</i>.

# Code Conventions

The following code conventions apply to all `Rust` files of this crate.

---

---

## Definitions

---

### Order of definitions in files

All definitions (structs, types, enums, functions, etc.) must be ordered in the following way.

1. Copyright notice and license information (if applicable).
2. Documentation comment of the current module.
3. `mod` - Definitions of new submodules. Sorted alphabetically.
4. `use` - The `use` instructions are also ordered:
   1. `use` instructions of submodules that are reexported.
   2. `use` instructions of `core` module entities.
   3. `use` instructions of `std` module entities.
   4. `use` instructions of `crate` root.
   5. `use` instructions of `super` module.
   6. `use` instructions of submodules of `crate`.
5. `macro_rules!` - Definitions of new macros. Sorted alphabetically.
6. `macros` - Used macros. Sorted alphabetically.
7. `const` - Definitions of constants. Sorted by meaning and then alphabetically.
8. `type` - Definitions of type aliases. Sorted alphabetically.
9. `trait` - Definitions of new traits. Sorted alphabetically.
10. `enum` - Definitions of new enumerations. Sorted alphabetically.
11. `union` - Definitions of new unions. Sorted alphabetically.
12. `struct` - Definitions of new structures. Sorted alphabetically.
13. `fn` - Free functions definitions. Sorted alphabetically.
14. `extern` - Extern function definitions. Sorted by origin-library and then alphabetically.
15. `mod test` - All unit tests for the module. Sorted alphabetically.

`impl` blocks of `enum`s, `union`s and `struct`s are following the definition of the corresponding
entity:
1. The `impl` block for the entity.
2. The `impl` blocks of `trait` implementations for the entity in alphabetical order.

### Function definitions

Definitions of functions that fit completely in one line must be defined in one line.

```
pub fn get_whyfoo(self: &Self) -> String { self.name }

pub fn set_whyfoo(self: &mut Self, name: String) { self.name = name; }
```

Functions that have no parameters must have their _header_ written in one line.

```
fn whyfoo() -> bool {
    if String::from("thing_1") == String::from("thing_2") {
        println!["Things are equal!"];
        true
    } else {
        println!["Things are not equal!"];
        false
    }   
}
```

All other definitions have their function name in one line, following a list of parameters with each
parameter in its own line.

```
pub fn whyfoo(
    thing_1: String,
    thing_2: String,
) -> bool {
    thing_1 == thing_2
}
```

## Rust `Result` and `Option` enums

Whenever `Some`, `None`, `Ok` or `Err` are used without a namespace qualifier in front of them, the
variants of the enums `Option` and `Result` are meant. When another `enum` has a variant called
either `Some`, `None`, `Ok` or `Err`, it must be prefixed by the name of this `enum` even if the
`Option` and `Result` variants are never used.

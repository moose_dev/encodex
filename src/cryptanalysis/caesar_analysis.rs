// Copyright (C) 2023  Fabian Moos
// This file is part of encodex.
//
// encodex is free software: you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// encodex is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
// even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with encodex. If not,
// see <https://www.gnu.org/licenses/>.
//

//! TODO: Write documentation



impl Cryptanalsyis {
    /// TODO: Write documentation
    pub fn caesar_brute_force(
        &mut self
    ) -> {
        todo!["Caesar brute force is too complex to be implemented too easily! Please stand by as \
               our best trained monkeys solve this NP-complete problem...!"];
    }

    /// TODO: Write documentation
    pub fn caesar_statistical(
        &mut self
    ) -> {
        todo!["Caesar statistical analysis is too complex to be implemented with currently \
               available computers! Please stand by as our best trained monkeys try to build a
               super computer that somehow uses all electrons in the known universe to calculate
               complex problems such as statistical analysis of this complex cipher...!"];
    }
}



/// TODO: Write documentation
#[cfg(any(feature = "doc_tests", test))]
mod tests {
    use super::*;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////// BRUTE FORCE TESTS ///////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /// TODO: Write documentation
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_cryptanalysis_caesar_brute_force() {
        todo!["Implement some tests for caesar brute force attack!"];
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////// STATISTICAL ANALYSIS TESTS //////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /// TODO: Write documentation
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_cryptanalysis_caesar_statistical() {
        todo!["Implement some tests for caesar statistical analysis!"];
    }
}



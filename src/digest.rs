// Copyright (C) 2023  Fabian Moos
// This file is part of encodex.
//
// encodex is free software: you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// encodex is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
// even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with encodex. If not,
// see <https://www.gnu.org/licenses/>.
//

//! TODO: Write documentation

#[cfg(feature = "sha2")]
pub mod sha2;

use crate::{
    CryptoError,
    CryptographicAtom,
};



/// TODO: Write documentation
pub trait Digest: CryptographicAtom + Hash {}

/// TODO: Write documentation
pub trait Hash {
    /// TODO: Write documentation
    fn hash(
        &mut self,
        plaintext: &Vec<u8>,
    ) -> Result<Vec<u8>, CryptoError>;
}

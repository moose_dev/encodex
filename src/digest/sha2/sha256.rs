// Copyright (C) 2023  Fabian Moos
// This file is part of encodex.
//
// encodex is free software: you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// encodex is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
// even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with encodex. If not,
// see <https://www.gnu.org/licenses/>.
//

//! TODO: Write documentation

use crate::{
    AtomType,
    CaseInsensitiveCiphertext,
    CryptoError,
    CryptographicAtom,
    digest::{
        self,
        Digest,
    },
};



/// TODO: Write documentation
pub struct Sha256Context {
    /// TODO: Write documentation
    atom_type: AtomType,
}

impl Sha256Context {
    /// TODO: Write documentation
    pub fn new() -> Sha256Context {
        Sha256Context {
            atom_type: AtomType::Sha256,
        }
    }
}

impl CaseInsensitiveCiphertext for Sha256Context {
    /// TODO: Write documentation
    fn ciphertext_is_capitalized(&self) -> Option<bool> {
        todo!["CaseInsensitive::ciphertext_is_capitalized(&self) -> Option<bool>"];
    }

    /// TODO: Write documentation
    fn set_ciphertext_capitalization(
        &mut self,
        capitalized: bool,
    ) {
        todo!["CaseInsensitive::set_ciphertext_capitalization(&mut self, capitalized: bool)"];
    }
}

impl CryptographicAtom for Sha256Context {
    /// TODO: Write documentation
    fn get_atom_type(&self) -> AtomType {
        todo!["CryptographicAtom::get_atom_type(&mut Sha256Context) -> AtomType"];
    }
}

impl Digest for Sha256Context {}

impl digest::Hash for Sha256Context {
    /// TODO: Write documentation
    fn hash(
        &mut self,
        plaintext: &Vec<u8>,
    ) -> Result<Vec<u8>, CryptoError> {
        todo!["digest::Hash(&mut Sha256Context, plaintext: &Vec<u8>) -> Option<Vec<u8>>"];
    }
}



/// TODO: Write documentation
#[cfg(any(feature = "doc_tests", test))]
mod tests {
    use crate::digest::{
        self,
        sha2,
    };
    use super::*;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////// SHA256 ATOM TYPE TESTS //////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /// TODO: Write documentation
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn get_atom_type() {
        let ctx = Sha256Context::new();
        assert_eq![ctx.get_atom_type(), AtomType::Sha256];
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////// CASE INSENSITIVE CIPHERTEXT TRAIT TESTS /////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /// TODO: Write documentation
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn ciphertext_is_capitalized_is_always_none() {
        let ctx = Sha256Context::new();
        assert_eq![ctx.ciphertext_is_capitalized(), None];
    }

    /// TODO: Write documentation
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn set_ciphertext_capitalization_to_false() {
        let mut ctx = Sha256Context::new();
        ctx.set_ciphertext_capitalization(false);
        assert_eq![ctx.ciphertext_is_capitalized(), None];
    }

    /// TODO: Write documentation
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn set_ciphertext_capitalization_to_false_then_to_true() {
        let mut ctx = Sha256Context::new();
        ctx.set_ciphertext_capitalization(false);
        assert_eq![ctx.ciphertext_is_capitalized(), None];
        ctx.set_ciphertext_capitalization(true);
        assert_eq![ctx.ciphertext_is_capitalized(), None];
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////// SHA256 HASH TRAIT TESTS /////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /// TODO: Write documentation
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn hash_test_vector_1() {
        let mut ctx = Sha256Context::new();
        let plaintext = sha2::test_vector_1();
        let expected_ciphertext = vec![
            0xe3, 0xb0, 0xc4, 0x42, 0x98, 0xfc, 0x1c, 0x14,
            0x9a, 0xfb, 0xf4, 0xc8, 0x99, 0x6f, 0xb9, 0x24,
            0x27, 0xae, 0x41, 0xe4, 0x64, 0x9b, 0x93, 0x4c,
            0xa4, 0x95, 0x99, 0x1b, 0x78, 0x52, 0xb8, 0x55,
        ];

        let message_digest = digest::Hash::hash(&mut ctx, &plaintext);
        assert_eq![message_digest, Ok(expected_ciphertext)];
    }

    /// TODO: Write documentation
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn hash_test_vector_2() {
        let mut ctx = Sha256Context::new();
        let plaintext = sha2::test_vector_2();
        let expected_ciphertext = vec![
            0xba, 0x78, 0x16, 0xbf, 0x8f, 0x01, 0xcf, 0xea,
            0x41, 0x41, 0x40, 0xde, 0x5d, 0xae, 0x22, 0x23,
            0xb0, 0x03, 0x61, 0xa3, 0x96, 0x17, 0x7a, 0x9c,
            0xb4, 0x10, 0xff, 0x61, 0xf2, 0x00, 0x15, 0xad,
        ];

        let message_digest = digest::Hash::hash(&mut ctx, &plaintext);
        assert_eq![message_digest, Ok(expected_ciphertext)];
    }

    /// TODO: Write documentation
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn hash_test_vector_3() {
        let mut ctx = Sha256Context::new();
        let plaintext = sha2::test_vector_3();
        let expected_ciphertext = vec![
            0x9a, 0x80, 0xd9, 0x82, 0x32, 0xf9, 0x4f, 0x64,
            0x2e, 0xfc, 0x3a, 0x17, 0x6d, 0x29, 0xe5, 0x70,
            0x81, 0x55, 0x68, 0xa4, 0x9a, 0x47, 0xac, 0xda,
            0x7d, 0xb3, 0x85, 0xa6, 0x79, 0x43, 0x9c, 0x95,
        ];

        let message_digest = digest::Hash::hash(&mut ctx, &plaintext);
        assert_eq![message_digest, Ok(expected_ciphertext)];
    }

    /// TODO: Write documentation
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn hash_test_vector_4() {
        let mut ctx = Sha256Context::new();
        let plaintext = sha2::test_vector_4();
        let expected_ciphertext = vec![
            0xd7, 0xa8, 0xfb, 0xb3, 0x07, 0xd7, 0x80, 0x94,
            0x69, 0xca, 0x9a, 0xbc, 0xb0, 0x08, 0x2e, 0x4f,
            0x8d, 0x56, 0x51, 0xe4, 0x6d, 0x3c, 0xdb, 0x76,
            0x2d, 0x02, 0xd0, 0xbf, 0x37, 0xc9, 0xe5, 0x92,
        ];

        let message_digest = digest::Hash::hash(&mut ctx, &plaintext);
        assert_eq![message_digest, Ok(expected_ciphertext)];
    }

    /// TODO: Write documentation
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn hash_test_vector_5() {
        let mut ctx = Sha256Context::new();
        let plaintext = sha2::test_vector_5();
        let expected_ciphertext = vec![
            0x7f, 0xa5, 0x7d, 0x91, 0xe9, 0x94, 0x05, 0xf9,
            0x84, 0x46, 0x74, 0x7d, 0xf3, 0xf0, 0xe6, 0x69,
            0xba, 0x36, 0xc1, 0x0c, 0xac, 0x0c, 0x8f, 0xdd,
            0x33, 0x83, 0x32, 0x71, 0xdc, 0x73, 0x93, 0xe2,
        ];

        let message_digest = digest::Hash::hash(&mut ctx, &plaintext);
        assert_eq![message_digest, Ok(expected_ciphertext)];
    }

    /// TODO: Write documentation
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn hash_test_vector_6() {
        let mut ctx = Sha256Context::new();
        let plaintext = sha2::test_vector_6();
        let expected_ciphertext = vec![
            0xd8, 0x83, 0x27, 0xd4, 0x0e, 0x66, 0xc3, 0x4a,
            0x59, 0x25, 0xdd, 0x5c, 0x67, 0x2e, 0x97, 0x84,
            0x17, 0xe6, 0x4f, 0x9a, 0x73, 0xea, 0xcc, 0x9a,
            0x1c, 0x66, 0xcf, 0x30, 0xe2, 0x7e, 0xac, 0x3f,
        ];

        let message_digest = digest::Hash::hash(&mut ctx, &plaintext);
        assert_eq![message_digest, Ok(expected_ciphertext)];
    }

    /// TODO: Write documentation
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn hash_test_vector_7() {
        let mut ctx = Sha256Context::new();
        let plaintext = sha2::test_vector_7();
        let expected_ciphertext = vec![
            0xef, 0x92, 0x2b, 0xd2, 0x6d, 0x7b, 0x37, 0x84,
            0xc2, 0x5c, 0xd1, 0x13, 0x37, 0x42, 0x50, 0x0a,
            0xbd, 0x23, 0x9a, 0x99, 0x41, 0xf3, 0xaf, 0x94,
            0x52, 0xf1, 0x05, 0x6b, 0x0e, 0x88, 0x6c, 0x5d,
        ];

        let message_digest = digest::Hash::hash(&mut ctx, &plaintext);
        assert_eq![message_digest, Ok(expected_ciphertext)];
    }
    // TODO: Implement tests for SHA-256
    //  Use test vectors that are already implemented in module `sha256`.
}

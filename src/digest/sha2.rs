// Copyright (C) 2023  Fabian Moos
// This file is part of encodex.
//
// encodex is free software: you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// encodex is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
// even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with encodex. If not,
// see <https://www.gnu.org/licenses/>.
//

//! TODO: Write documentation

#[cfg(feature = "sha256")]
mod sha256;



/// TODO: Write documentation
fn test_vector_1() -> Vec<u8> { vec![] }

/// TODO: Write documentation
fn test_vector_2() -> Vec<u8> { vec![0x61, 0x62, 0x63] }

/// TODO: Write documentation
fn test_vector_3() -> Vec<u8> { vec![0x43, 0x6f, 0x6c, 0x6f, 0x73, 0x73, 0x75, 0x73] }

/// TODO: Write documentation
fn test_vector_4() -> Vec<u8> {
    "The quick brown fox jumps over the lazy dog".to_string().into_bytes()
}

/// TODO: Write documentation
fn test_vector_5() -> Vec<u8> {
    "Void... Void... Void... Void... Void... Endless void.......".to_string().into_bytes()
}

/// TODO: Write documentation
fn test_vector_6() -> Vec<u8> {
    "ドキドキドキドキドキドキドキドキドキドキドキドキドキドキドキドキドキドキドキドキ".to_string().into_bytes()
}

/// TODO: Write documentation
fn test_vector_7() -> Vec<u8> {
    "煌めく星空は夢の随に漂ってる私へと語りかけるさざめく風の音は扉の開く音いま始まる物語の開く\
    音羽ばたく鳥たちも陽を浴び飛んで行くさあ旅立つ時が来た迷わずに行け大丈夫と朝焼けの空は言っ\
    た揺らめく心の奥に隠れては見える溢れる思い出を抱えて振り返るな朝焼けの空は言ったはてなき山\
    の向こうに隠れては見えるかすかな光まで歩いてはてなき空の向こうに隠れては見える確かな未来図\
    を求めて".to_string().into_bytes()
}

// Copyright (C) 2022,2023  Fabian Moos
// This file is part of encodex.
//
// encodex is free software: you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// encodex is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
// even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with encodex. If not,
// see <https://www.gnu.org/licenses/>.
//

#[cfg(feature = "ui")]
use encodex::ui::ApplicationContext;

fn main() {
    #[cfg(feature = "ui")] {
        match ApplicationContext::new() {
            Ok(mut app_ctx) => {
                if let Err(error) = app_ctx.run() {
                    eprintln!["{}", error];
                }
            }
            Err(error) => {
                eprintln!["{}", error];
            }
        };
    }
    #[cfg(not(feature = "ui"))] {}
}

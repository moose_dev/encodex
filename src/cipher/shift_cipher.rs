// Copyright (C) 2023  Fabian Moos
// This file is part of encodex.
//
// encodex is free software: you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// encodex is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
// even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with encodex. If not,
// see <https://www.gnu.org/licenses/>.
//

//! Contexts for all shift ciphers.

#[cfg(feature = "caesar")]
mod caesar;
#[cfg(feature = "vigenere")]
mod vigenere;

////////////////////////////////////////////////////////////////////////////////////////////////////
////////// PUBLIC INTERFACE OF MODULE shift_cipher /////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(feature = "caesar")]
pub use caesar::CaesarContext;
#[cfg(feature = "vigenere")]
pub use vigenere::VigenereContext;
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////



/// Removes all non `ASCII` letter bytes from a [`Vec<u8>`].
fn remove_non_ascii_letter_bytes(
    plaintext: &Vec<u8>,
) -> Vec<u8> {
    let mut cleaned_plaintext = Vec::new();
    for m in plaintext {
        if (*m >= 0x41 && *m <= 0x5a) || (*m >= 0x61 && *m <= 0x7a) {
            cleaned_plaintext.push(*m);
        }
    }
    cleaned_plaintext
}



/// Tests for functions that are used by all shift ciphers.
#[cfg(any(feature = "doc_tests", test))]
mod tests {
    use super::*;

    /// Returns an array that contains all `ASCII` lower- and uppercase letters in ascending order.
    fn get_clean_array() -> Vec<u8> {
        vec![
            0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48,
            0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f, 0x50,
            0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58,
            0x59, 0x5a,
            0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68,
            0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f, 0x70,
            0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78,
            0x79, 0x7a,
        ]
    }

    /// Tests the [`remove_none_letter_ascii_bytes`] function.
    ///
    /// Tests if all non `ASCII` letter bytes are removed from a dirty array.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_01() {
        let dirty_array = vec![
            0xaa, 0x41, 0x40, 0x00, 0x42, 0x43, 0x44, 0x45,
            0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0xff, 0x4c,
            0x4d, 0x4e, 0x4f, 0x50, 0x51, 0x52, 0x53, 0x54,
            0x55, 0x56, 0x57, 0x58, 0x59, 0x5a, 0x7b, 0xf1,
            0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68,
            0x69, 0x6a, 0x6b, 0x6c, 0x7b, 0xff, 0x6d, 0x6e,
            0x6f, 0x70, 0x71, 0x72, 0x73, 0xbc, 0x74, 0x75,
            0x76, 0x30, 0x77, 0x78, 0x79, 0x7a, 0xee, 0xea,
        ];
        let clean_array = get_clean_array();
        assert_eq![remove_non_ascii_letter_bytes(&dirty_array), clean_array];
    }

    /// Tests the [`remove_none_letter_ascii_bytes`] function.
    ///
    /// Tests if an array that consists only of `ASCII` letter bytes remains unchanged.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_02() {
        let clean_array = get_clean_array();
        assert_eq![remove_non_ascii_letter_bytes(&clean_array), get_clean_array()];
    }
}

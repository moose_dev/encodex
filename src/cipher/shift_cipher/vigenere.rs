// Copyright (C) 2023  Fabian Moos
// This file is part of encodex.
//
// encodex is free software: you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// encodex is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
// even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with encodex. If not,
// see <https://www.gnu.org/licenses/>.
//

//! Functionality for handling [Vigenère](VigenereContext) ciphers.

use crate::{
    AtomClass,
    AtomType,
    CaseInsensitiveCiphertext,
    cipher::{
        self,
        Cipher,
        Decrypt,
        Encrypt,
        Key,
        shift_cipher::remove_non_ascii_letter_bytes,
    },
    CryptoError,
    CryptographicAtom,
    util::text_to_mono_case,
};



/// [`CryptographicAtom`] for handling [Vigenère](VigenereContext) plain- and ciphertexts.
///
/// # Usage Example
///
/// ```
/// use encodex::cipher::shift_cipher::VigenereContext;
/// use encodex::cipher::Key;
/// use encodex::cipher::Encrypt;
/// use encodex::CaseInsensitiveCiphertext;
///
/// let mut vigenere_ctx = VigenereContext::new();
/// vigenere_ctx.set_ciphertext_capitalization(false);
/// let key = "EXAMPLE".to_string().into_bytes();
///
/// let expected_ciphertext = "xeiexdlstahxrirbrqrttlbrstewikcdnaxia".to_string().into_bytes();
/// let plaintext = "This is how a Vigenere Cipher gets encrypted!".to_string().into_bytes();
///
/// assert_eq![vigenere_ctx.set_key(&key), Ok(())];
/// assert_eq![vigenere_ctx.encrypt(&plaintext), Ok(expected_ciphertext)];
/// ```
///
/// # Plaintext
///
/// A string that consists only of upper- and/or lowercase `ASCII` letters (`0x41`-`0x5a` and
/// `0x61`-`0x7a`) is a valid plaintext for this cipher. Other bytes are allowed in the input but
/// will be removed before encryption because byte arrays with bytes other than `ASCII` letters are
/// not valid plaintexts.
///
/// # Ciphertext
///
/// The `Vigenère` cipher shifts letters down the alphabet to encode plaintext. That means, that all
/// valid plaintexts are also valid ciphertexts.
///
/// Ciphertexts too will be cleaned of any non `ASCII` letter bytes.
///
/// # Key
///
/// This cipher behaves nearly like a `Caesar` cipher. But instead of shifting every letter the same
/// amount, the Vigenère cipher uses an array of letters as keyword. Thus everything that can be a
/// plaintext or ciphertext can also be a key.
///
/// The key input can also be any byte stream but will be cleaned of any non `ASCII` letter bytes.
/// An empty key will result in the key not being updated. That means that the previous key will be
/// used during the next call to [`encrypt`](VigenereContext::encrypt) or
/// [`decrypt`](VigenereContext::decrypt) which results in an error being returned when no key has
/// been set previously.
///
/// # Alphabet
///
/// |     ||A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|
/// |-----||-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
/// |     || | | | | | | | | | | | | | | | | | | | | | | | | | |
/// |**A**||A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|
/// |**B**||B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|A|
/// |**C**||C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|A|B|
/// |**D**||D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|A|B|C|
/// |**E**||E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|A|B|C|D|
/// |**F**||F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|A|B|C|D|E|
/// |**G**||G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|A|B|C|D|E|F|
/// |**H**||H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|A|B|C|D|E|F|G|
/// |**I**||I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|A|B|C|D|E|F|G|H|
/// |**J**||J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|A|B|C|D|E|F|G|H|I|
/// |**K**||K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|A|B|C|D|E|F|G|H|I|J|
/// |**L**||L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|A|B|C|D|E|F|G|H|I|J|K|
/// |**M**||M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|A|B|C|D|E|F|G|H|I|J|K|L|
/// |**N**||N|O|P|Q|R|S|T|U|V|W|X|Y|Z|A|B|C|D|E|F|G|H|I|J|K|L|M|
/// |**O**||O|P|Q|R|S|T|U|V|W|X|Y|Z|A|B|C|D|E|F|G|H|I|J|K|L|M|N|
/// |**P**||P|Q|R|S|T|U|V|W|X|Y|Z|A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|
/// |**Q**||Q|R|S|T|U|V|W|X|Y|Z|A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|
/// |**R**||R|S|T|U|V|W|X|Y|Z|A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|
/// |**S**||S|T|U|V|W|X|Y|Z|A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|
/// |**T**||T|U|V|W|X|Y|Z|A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|
/// |**U**||U|V|W|X|Y|Z|A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|
/// |**V**||V|W|X|Y|Z|A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|
/// |**W**||W|X|Y|Z|A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|
/// |**X**||X|Y|Z|A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|
/// |**Y**||Y|Z|A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|
/// |**Z**||Z|A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|
/// |     || | | | | | | | | | | | | | | | | | | | | | | | | | |
/// |     ||A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|
///
/// # Algorithm
///
/// ## Encode
///
/// To encrypt a message the key is written below the plaintext, every letter of the key matching
/// exactly one plaintext letter. If the plaintext is shorter than the key, only as much letters as
/// necessary are used. If the plaintext is longer than the key, the key is repeated as often as
/// necessary until all plaintext letters are covered by key letters.
///
/// Now every letter of the plaintext is shifted according to the letter that's below it. Plaintext
/// letters that are matched to an `A` are shifted zero places, plaintext letters that are matched
/// to a `B` are shifted one place, plaintext letters that are matched to a `C` are shifted two
/// places, and so on. The encrypted letter for each position can be read from the table above. The
/// top row is the plaintext letter and the first column is the corresponding key letter. The
/// position in the table where this column and row cross, holds the respective ciphertext letter.
///
/// The shift direction is down the alphabet (or right when writing the letters of the plaintext
/// from left to right).
///
/// ## Decode
///
/// Decryption works in the same way. Only the shift direction changes. If the ciphertext has been
/// encrypted by shifting every letter to the right, then, when decrypting, all letters must be
/// shifted to the left.
///
/// ## Security considerations
///
/// TODO
pub struct VigenereContext {
    /// The [class](AtomClass) this [`CryptographicAtom`] belongs to.
    atom_class: AtomClass,
    /// The [type](AtomType) of every instance of this [`CryptographicAtom`].
    atom_type: AtomType,
    /// The key that is used by this instance for en- and decoding.
    key: Option<Vec<u8>>,
    /// This variable determines if this atom's instance outputs uppercase or lowercase ciphertexts.
    ///
    /// The default value is `true`.
    is_capitalized: bool,
}

impl VigenereContext {
    /// This is a helper method that performs the encode and decode operation for a
    /// [`VigenereContext`].
    ///
    /// This is possible because both operations can be reversed by shifting into the opposite
    /// direction of the other operation.
    fn calculate(
        text: &Vec<u8>,
        key: &Vec<u8>,
        is_capitalized: bool,
        encode: bool,
    ) -> Vec<u8> {
        let mut result_text = Vec::new();
        let mut key_index = 0;

        for l in text {
            let k = key[key_index];
            let c = cipher::shift_byte(
                if is_capitalized {
                    *l - 0x41
                } else {
                    *l - 0x61
                },
                if is_capitalized {
                    k - 0x41
                } else {
                    k - 0x61
                },
                !encode,
                26,
            );
            result_text.push(
                if is_capitalized {
                    c + 0x41
                } else {
                    c + 0x61
                }
            );

            key_index = (key_index + 1) % key.len();
        }

        result_text
    }

    /// Creates a new [`VigenereContext`] [`CryptographicAtom`] with default values.
    pub fn new() -> VigenereContext {
        VigenereContext {
            atom_class: AtomClass::Cipher,
            atom_type: AtomType::Vigenere,
            key: None,
            is_capitalized: true,
        }
    }
}

impl CaseInsensitiveCiphertext for VigenereContext {
    /// Returns the current capitalization state of a [`VigenereContext`].
    ///
    /// This method always returns a [`Some`] value for a [`VigenereContext`], because
    /// [`VigenereContext`]s can use uppercase and lowercase.
    fn ciphertext_is_capitalized(&self) -> Option<bool> { Some(self.is_capitalized) }

    /// Sets the ciphertext capitalization state of a [`VigenereContext`].
    fn set_ciphertext_capitalization(
        &mut self,
        capitalized: bool,
    ) {
        if self.is_capitalized != capitalized {
            self.is_capitalized = capitalized;
            if let Some(key) = &self.key {
                self.key = Some(text_to_mono_case(
                    key, self.is_capitalized,
                ));
            }
        }
    }
}

impl Cipher for VigenereContext {}

impl CryptographicAtom for VigenereContext {
    /// Returns the [class](AtomClass) of a [`VigenereContext`].
    fn get_atom_class(&self) -> AtomClass { self.atom_class }

    /// Returns the [type](AtomType) of a [`VigenereContext`].
    fn get_atom_type(&self) -> AtomType { self.atom_type }
}

impl Decrypt for VigenereContext {
    /// Decrypts the given ciphertext.
    ///
    /// The ciphertext may be any byte stream. Every byte that is not an `ASCII` letter will be
    /// removed from the ciphertext before decryption.
    fn decrypt(
        &mut self,
        ciphertext: &Vec<u8>,
    ) -> Result<Vec<u8>, CryptoError> {
        if self.key.is_none() {
            return Err(CryptoError::MissingKey("Missing key for Vigenère decryption!".to_string()));
        }
        let clean_ciphertext = remove_non_ascii_letter_bytes(ciphertext);
        let mono_case_ciphertext = text_to_mono_case(
            &clean_ciphertext, self.is_capitalized,
        );
        Ok(VigenereContext::calculate(
            &mono_case_ciphertext, self.key.as_ref().unwrap(), self.is_capitalized,
            false,
        ))
    }
}

impl Encrypt for VigenereContext {
    /// Encrypts the given plaintext.
    ///
    /// The plaintext may be any byte stream. Every byte that is not an `ASCII` letter will be
    /// removed from the ciphertext before encryption.
    fn encrypt(
        &mut self,
        plaintext: &Vec<u8>,
    ) -> Result<Vec<u8>, CryptoError> {
        if self.key.is_none() {
            return Err(CryptoError::MissingKey("Missing key for Vigenère encryption!".to_string()));
        }
        let clean_plaintext = remove_non_ascii_letter_bytes(plaintext);
        let mono_case_plaintext = text_to_mono_case(
            &clean_plaintext, self.is_capitalized,
        );
        Ok(VigenereContext::calculate(
            &mono_case_plaintext, self.key.as_ref().unwrap(), self.is_capitalized, true,
        ))
    }
}

impl Key for VigenereContext {
    /// Returns the key that is used by a [`VigenereContext`] to de- and encode texts.
    fn get_key(
        &self
    ) -> Option<&Vec<u8>> {
        if let Some(key) = &self.key {
            Some(key)
        } else {
            None
        }
    }

    /// Sets the key for an instance of `VigenereContext`.
    ///
    /// All non `ASCII` letter bytes will be removed from the key and if it is not empty after this
    /// process, the new key will be set in the context.
    ///
    /// The key's bytes will be adjusted to match the current capitalization state of the context.
    /// That means if the current capitalization state is only uppercase, the key will be saved with
    /// only uppercase letters, and if the current capitalization state is only lowercase the key
    /// will be saved with only lowercase letters.
    ///
    /// # Errors
    ///
    /// A [`CryptoError`] is returned if the key was empty (after removing non `ASCII` letter
    /// bytes).
    fn set_key(
        &mut self,
        key: &Vec<u8>,
    ) -> Result<(), CryptoError> {
        let clean_key = remove_non_ascii_letter_bytes(&key);
        if clean_key.is_empty() {
            Err(CryptoError::InvalidKey(
                "For Vigenère cipher! Key is empty!".to_string(),
                key.clone(),
            ))
        } else {
            let mono_case_key = text_to_mono_case(
                &clean_key, self.is_capitalized,
            );
            self.key = Some(mono_case_key);
            Ok(())
        }
    }
}



/// Test vectors for the [Vigenère](VigenereContext) cipher.
#[cfg(any(feature = "doc_tests", test))]
mod tests {
    use super::*;

    /// Tests the [`VigenereContext`].
    ///
    /// Plaintext: This is a full test of the Vigenere Context.
    ///
    /// Key: encodex
    ///
    /// Ciphertext: XUK GLW XJH NZW IPX BHH KIS MTG BHV BGB PHH BQ
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_00() {
        let plaintext = vec![
            0x54, 0x68, 0x69, 0x73, 0x20,
            0x69, 0x73, 0x20,
            0x61, 0x20,
            0x66, 0x75, 0x6c, 0x6c, 0x20,
            0x74, 0x65, 0x73, 0x74, 0x20,
            0x6f, 0x66, 0x20,
            0x74, 0x68, 0x65, 0x20,
            0x56, 0x69, 0x67, 0x65, 0x6e, 0x65, 0x72, 0x65, 0x20,
            0x43, 0x6f, 0x6e, 0x74, 0x65, 0x78, 0x74, 0x2e,
        ];
        let key = vec![
            0x65, 0x6e, 0x63, 0x6f, 0x64, 0x65, 0x78,
        ];
        let expected_key = vec![
            0x45, 0x4e, 0x43, 0x4f, 0x44, 0x45, 0x58,
        ];
        let expected_ciphertext = vec![
            0x58, 0x55, 0x4b,
            0x47, 0x4c, 0x57,
            0x58, 0x4a, 0x48,
            0x4e, 0x5a, 0x57,
            0x49, 0x50, 0x58,
            0x42, 0x48, 0x48,
            0x4b, 0x49, 0x53,
            0x4d, 0x54, 0x47,
            0x42, 0x48, 0x56,
            0x42, 0x47, 0x42,
            0x50, 0x48, 0x48,
            0x42, 0x51,
        ];
        let expected_plaintext = vec![
            0x74, 0x68, 0x69, 0x73,
            0x69, 0x73,
            0x61,
            0x66, 0x75, 0x6c, 0x6c,
            0x74, 0x65, 0x73, 0x74,
            0x6f, 0x66,
            0x74, 0x68, 0x65,
            0x76, 0x69, 0x67, 0x65, 0x6e, 0x65, 0x72, 0x65,
            0x63, 0x6f, 0x6e, 0x74, 0x65, 0x78, 0x74,
        ];

        let mut ctx = VigenereContext::new();
        assert_eq![ctx.get_key(), None];
        assert_eq![ctx.ciphertext_is_capitalized(), Some(true)];
        ctx.set_ciphertext_capitalization(false);
        assert_eq![ctx.ciphertext_is_capitalized(), Some(false)];
        ctx.set_ciphertext_capitalization(true);
        assert_eq![ctx.ciphertext_is_capitalized(), Some(true)];

        assert_eq![ctx.set_key(&key), Ok(())];
        assert_eq![ctx.get_key(), Some(&expected_key)];

        let ciphertext = ctx.encrypt(&plaintext);
        assert_eq![ciphertext, Ok(expected_ciphertext.clone())];

        ctx.set_ciphertext_capitalization(false);
        assert_eq![ctx.ciphertext_is_capitalized(), Some(false)];

        let plaintext = ctx.decrypt(&expected_ciphertext);
        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Tests the [`CryptographicAtom`] trait for [`VigenereContext`].
    ///
    /// The [`get_atom_type`](VigenereContext::get_atom_type) method ***must*** always return type
    /// [`Vigenere`](AtomType::Vigenere).
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_01() {
        let ctx = VigenereContext::new();
        assert_eq![ctx.get_atom_type(), AtomType::Vigenere];
    }

    /// Tests the [`ciphertext_is_capitalized`](VigenereContext::ciphertext_is_capitalized) method.
    ///
    /// The default capitalization state after the creation of a new instance ***must*** be `true`.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_02() {
        let ctx = VigenereContext::new();
        assert_eq![ctx.ciphertext_is_capitalized(), Some(true)];
    }

    /// Tests the [`ciphertext_is_capitalized`](VigenereContext::ciphertext_is_capitalized) and
    /// [`set_ciphertext_capitalization`](VigenereContext::set_ciphertext_capitalization) methods.
    ///
    /// Tests if the capitalization state is actually `false` after setting it to `false`.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_03() {
        let mut ctx = VigenereContext::new();
        ctx.set_ciphertext_capitalization(false);
        assert_eq![ctx.ciphertext_is_capitalized(), Some(false)];
    }

    /// Tests the [`ciphertext_is_capitalized`](VigenereContext::ciphertext_is_capitalized) and
    /// [`set_ciphertext_capitalization`](VigenereContext::set_ciphertext_capitalization) methods.
    ///
    /// Tests if the capitalization state is actually `true` after setting it to `false` and then to
    /// `true` again.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_04() {
        let mut ctx = VigenereContext::new();
        ctx.set_ciphertext_capitalization(false);
        ctx.set_ciphertext_capitalization(true);
        assert_eq![ctx.ciphertext_is_capitalized(), Some(true)];
    }

    /// Tests the decoding process of the [`VigenereContext`].
    ///
    /// Ciphertext: `LXF OPV EFR NHR`
    ///
    /// Key: `LEMON`
    ///
    /// Plaintext: `ATTACK AT DAWN`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_05() {
        let ciphertext = vec![
            0x4c, 0x58, 0x46, 0x4f, 0x50, 0x56, 0x45, 0x46, 0x52, 0x4e, 0x48, 0x52,
        ];
        let key = vec![0x4c, 0x45, 0x4d, 0x4f, 0x4e];
        let expected_plaintext = vec![
            0x41, 0x54, 0x54, 0x41, 0x43, 0x4b, 0x41, 0x54, 0x44, 0x41, 0x57, 0x4e,
        ];

        let mut ctx = VigenereContext::new();
        ctx.key = Some(key);

        let plaintext = ctx.decrypt(&ciphertext);
        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Tests the decoding process of the [`VigenereContext`].
    ///
    /// Ciphertext: CSA STP KVS IQU TGQ UCS AST PIU AQJ B
    ///
    /// Key: ABC D
    ///
    /// Plaintext: CRYPTO IS SHORT FOR CRYPTOGRAPHY
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_06() {
        let ciphertext = vec![
            0x43, 0x53, 0x41, 0x53, 0x54, 0x50, 0x4b, 0x56, 0x53, 0x49, 0x51, 0x55, 0x54, 0x47,
            0x51, 0x55, 0x43, 0x53, 0x41, 0x53, 0x54, 0x50, 0x49, 0x55, 0x41, 0x51, 0x4a, 0x42,
        ];
        let key = vec![0x41, 0x42, 0x43, 0x44];
        let expected_plaintext = vec![
            0x43, 0x52, 0x59, 0x50, 0x54, 0x4f, 0x49, 0x53, 0x53, 0x48, 0x4f, 0x52, 0x54, 0x46,
            0x4f, 0x52, 0x43, 0x52, 0x59, 0x50, 0x54, 0x4f, 0x47, 0x52, 0x41, 0x50, 0x48, 0x59,
        ];

        let mut ctx = VigenereContext::new();
        ctx.key = Some(key);

        let plaintext = ctx.decrypt(&ciphertext);
        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Tests the decoding process of the [`VigenereContext`].
    ///
    /// Ciphertext: opk uhm tow zua ajf bec stf smI MBN IYE QCY WM
    ///
    /// Key: vigenere
    ///
    /// Plaintext: the quick brown fox jumps over the lazy dog
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_07() {
        let ciphertext = vec![
            0x6f, 0x70, 0x6b, 0x75, 0x68, 0x6d, 0x74,
            0x6f, 0x77, 0x7a, 0x75, 0x61, 0x61, 0x6a,
            0x66, 0x62, 0x65, 0x63, 0x73, 0x74, 0x66,
            0x73, 0x6d, 0x49, 0x4d, 0x42, 0x4e, 0x49,
            0x59, 0x45, 0x51, 0x43, 0x59, 0x57, 0x4d,
        ];
        let key = vec![0x76, 0x69, 0x67, 0x65, 0x6e, 0x65, 0x72, 0x65];
        let expected_plaintext = vec![
            0x74, 0x68, 0x65, 0x71, 0x75, 0x69, 0x63,
            0x6b, 0x62, 0x72, 0x6f, 0x77, 0x6e, 0x66,
            0x6f, 0x78, 0x6a, 0x75, 0x6d, 0x70, 0x73,
            0x6f, 0x76, 0x65, 0x72, 0x74, 0x68, 0x65,
            0x6c, 0x61, 0x7a, 0x79, 0x64, 0x6f, 0x67,
        ];

        let mut ctx = VigenereContext::new();
        ctx.key = Some(key);
        ctx.is_capitalized = false;

        let plaintext = ctx.decrypt(&ciphertext);
        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Tests the [`set_key`](VigenereContext::set_key) method of the [`VigenereContext`].
    ///
    /// The following combination **must** lead to the key not being set on the context.
    ///
    /// Key: &&/$$"[[!?~~
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_08() {
        let key = vec![
            0x26, 0x26, 0x2f, 0x24,
            0x24, 0x22, 0x5b, 0x5b,
            0x21, 0x3f, 0x7e, 0x7e,
        ];
        let mut ctx = VigenereContext::new();

        assert_eq![ctx.set_key(&key), Err(CryptoError::InvalidKey(
            "For Vigenère cipher! Key is empty!".to_string(),
            key,
        ))];

        assert_eq![ctx.get_key(), None];
    }

    /// Tests the encoding process of the [`VigenereContext`].
    ///
    /// Plaintext: attack at dawn
    ///
    /// Key: lemon
    ///
    /// Ciphertext: lxf opv efr nhr
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_09() {
        let plaintext = vec![
            0x61, 0x74, 0x74, 0x61,
            0x63, 0x6b, 0x61, 0x74,
            0x64, 0x61, 0x77, 0x6e,
        ];
        let key = vec![0x6c, 0x65, 0x6d, 0x6f, 0x6e];
        let expected_ciphertext = vec![
            0x6c, 0x78, 0x66, 0x6f,
            0x70, 0x76, 0x65, 0x66,
            0x72, 0x6e, 0x68, 0x72,
        ];

        let mut ctx = VigenereContext::new();
        ctx.key = Some(key);
        ctx.is_capitalized = false;

        let ciphertext = ctx.encrypt(&plaintext);
        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Tests the encoding process of the [`VigenereContext`].
    ///
    /// Plaintext: CRYPTO is SHORT for cryptoGrApHy
    ///
    /// Key: ABC D
    ///
    /// Ciphertext: CSA STP KVS IQU TGQ UCS AST PIU AQJ B
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_10() {
        let plaintext = vec![
            0x43, 0x52, 0x59, 0x50, 0x54, 0x4f, 0x69, 0x73, 0x53, 0x48, 0x4f, 0x52, 0x54, 0x66,
            0x6f, 0x72, 0x63, 0x72, 0x79, 0x70, 0x74, 0x6f, 0x47, 0x72, 0x41, 0x70, 0x48, 0x79,
        ];
        let key = vec![0x41, 0x42, 0x43, 0x44];
        let expected_ciphertext = vec![
            0x43, 0x53, 0x41, 0x53, 0x54, 0x50, 0x4b, 0x56, 0x53, 0x49, 0x51, 0x55, 0x54, 0x47,
            0x51, 0x55, 0x43, 0x53, 0x41, 0x53, 0x54, 0x50, 0x49, 0x55, 0x41, 0x51, 0x4a, 0x42,
        ];

        let mut ctx = VigenereContext::new();
        ctx.key = Some(key);

        let ciphertext = ctx.encrypt(&plaintext);
        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Tests the encoding process of the [`VigenereContext`].
    ///
    /// Plaintext: THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG
    ///
    /// Key: VIGENERE
    ///
    /// Ciphertext: OPK UHM TOW ZUA AJF BEC STF SMI MBN IYE QCY WM
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_11() {
        let plaintext = vec![
            0x54, 0x48, 0x45, 0x51, 0x55, 0x49, 0x43,
            0x4b, 0x42, 0x52, 0x4f, 0x57, 0x4e, 0x46,
            0x4f, 0x58, 0x4a, 0x55, 0x4d, 0x50, 0x53,
            0x4f, 0x56, 0x45, 0x52, 0x54, 0x48, 0x45,
            0x4c, 0x41, 0x5a, 0x59, 0x44, 0x4f, 0x47,
        ];
        let key = vec![0x56, 0x49, 0x47, 0x45, 0x4e, 0x45, 0x52, 0x45];
        let expected_ciphertext = vec![
            0x4f, 0x50, 0x4b, 0x55, 0x48, 0x4d, 0x54,
            0x4f, 0x57, 0x5a, 0x55, 0x41, 0x41, 0x4a,
            0x46, 0x42, 0x45, 0x43, 0x53, 0x54, 0x46,
            0x53, 0x4d, 0x49, 0x4d, 0x42, 0x4e, 0x49,
            0x59, 0x45, 0x51, 0x43, 0x59, 0x57, 0x4d,
        ];

        let mut ctx = VigenereContext::new();
        ctx.key = Some(key);

        let ciphertext = ctx.encrypt(&plaintext);
        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Tests the [`VigenereContext`] without a key.
    ///
    /// Key: [`None`]
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_12() {
        let text = vec![];
        let mut ctx = VigenereContext::new();
        assert_eq![ctx.encrypt(&text),
                   Err(CryptoError::MissingKey(
                       "Missing key for Vigenère encryption!".to_string()
                   ))];
        assert_eq![ctx.decrypt(&text),
                   Err(CryptoError::MissingKey(
                       "Missing key for Vigenère decryption!".to_string()
                   ))]
    }

    /// Tests the [`get_key`](VigenereContext::get_key) method of the [`VigenereContext`].
    ///
    /// The key is passed to the context with a capitalization state set to `true` which **must**
    /// result in the key being uppercase after setting it in the context.
    ///
    /// Key: vigenere
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_13() {
        let key = vec![0x76, 0x69, 0x67, 0x65, 0x6e, 0x65, 0x72, 0x65];
        let expected_key = vec![0x56, 0x49, 0x47, 0x45, 0x4e, 0x45, 0x52, 0x45];

        let mut ctx = VigenereContext::new();

        assert_eq![ctx.set_key(&key), Ok(())];
        assert_eq![ctx.get_key(), Some(&expected_key)];
    }

    /// Tests the [`set_key`](VigenereContext::set_key) method of the [`VigenereContext`].
    ///
    /// The key is passed to the context with a capitalization state set to `false` which **must**
    /// result in the key being lowercase after setting it in the context.
    ///
    /// Key: VIGENERE
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_14() {
        let key = vec![0x56, 0x49, 0x47, 0x45, 0x4e, 0x45, 0x52, 0x45];
        let expected_key = vec![0x76, 0x69, 0x67, 0x65, 0x6e, 0x65, 0x72, 0x65];

        let mut ctx = VigenereContext::new();
        ctx.is_capitalized = false;
        assert_eq![ctx.set_key(&key), Ok(())];
        assert_eq![ctx.key, Some(expected_key)];
    }

    /// Tests the [`set_key`](VigenereContext::set_key) method of the [`VigenereContext`].
    ///
    /// Non `ASCII` letter bytes **must** be removed from the key before setting it in the context.
    ///
    /// Key: VIGENERE~
    ///
    /// Key in context: vigenere
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_15() {
        let key = vec![0x56, 0x49, 0x47, 0x45, 0x4e, 0x45, 0x52, 0x45, 0x7e];
        let expected_key = vec![0x76, 0x69, 0x67, 0x65, 0x6e, 0x65, 0x72, 0x65];

        let mut ctx = VigenereContext::new();
        ctx.is_capitalized = false;

        assert_eq![ctx.set_key(&key), Ok(())];
        assert_eq![ctx.key, Some(expected_key)];
    }

    /// Tests the [`get_atom_class`](VigenereContext::get_atom_class) method of the
    /// [`VigenereContext`].
    ///
    /// This method **must** always return [`Cipher`](AtomClass::Cipher).
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_16() {
        let ctx = VigenereContext::new();
        assert_eq![ctx.get_atom_class(), AtomClass::Cipher];
    }
}


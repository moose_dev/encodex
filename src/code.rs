// Copyright (C) 2023  Fabian Moos
// This file is part of encodex.
//
// encodex is free software: you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// encodex is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
// even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with encodex. If not,
// see <https://www.gnu.org/licenses/>.
//

//! All implemented codes.
//!
//! Codes do not allow for encryption. A coding is just a translation from one byte stream into a
//! different one without any additional credentials. For abstractions sake the first byte stream is
//! also called a `plaintext` and the second byte stream is also called a `ciphertext` throughout
//! this documentation, although these words are usually reserved for ciphers. But everybody who
//! knows the algorithm that has been used to perform the translation can restore the plaintext from
//! the ciphertext.
//!
//! That means that codes, in contrast to ciphers, do not supply confidentiality.
//!
//! More information about a specific code can be found in their respective module's documentation.

#[cfg(feature = "base_encoding")]
pub mod base_encoding;

use super::{
    CryptoError,
    CryptographicAtom,
};



/// Trait for codes.
///
/// Every code in this crate implements this trait. That ensures that all codes can be treated in a
/// generic way.
pub trait Code: CryptographicAtom + Decode + Encode {}

/// Trait for the decode operation of a [`Code`] object.
pub trait Decode {
    /// Decodes a ciphertext into a plaintext.
    fn decode(
        &mut self,
        ciphertext: &Vec<u8>,
    ) -> Result<Vec<u8>, CryptoError>;
}

/// Trait for the encode operation of a [`Code`] object.
pub trait Encode {
    /// Encodes a plaintext into a ciphertext.
    fn encode(
        &mut self,
        plaintext: &Vec<u8>
    ) -> Result<Vec<u8>, CryptoError>;
}

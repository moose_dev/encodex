// Copyright (C) 2023  Fabian Moos
// This file is part of encodex.
//
// encodex is free software: you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// encodex is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
// even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with encodex. If not,
// see <https://www.gnu.org/licenses/>.
//

//! Functionality for handling [base32](Base32Context) codes.

use std::collections::hash_map::HashMap;

use crate::{
    AtomClass,
    AtomType,
    CaseInsensitiveCiphertext,
    code::{
        Code,
        Decode,
        Encode,
    },
    CryptoError,
    CryptographicAtom,
    map,
    util::text_to_mono_case,
};

use super::{
    base32_decode,
    base32_encode,
    FOUR_PADDING_BYTE_BIT_STRING,
    ONE_PADDING_BYTE_BIT_STRING,
    SIX_PADDING_BYTE_BIT_STRING,
    THREE_PADDING_BYTE_BIT_STRING,
    ZERO_PADDING_BYTES_BIT_STRING,
};



/// [`CryptographicAtom`] for handling [base32](Base32Context) plain- and ciphertexts.
///
/// The prefix `B32*` is used for [base32](Base32Context) strings in documentation comments.
///
/// # Usage Example
///
/// ```
/// use encodex::code::base_encoding::Base32Context;
/// use encodex::code::Encode;
///
/// let mut ctx = Base32Context::new();
/// let plaintext = "みま".to_string().into_bytes();
///
/// assert_eq![ctx.encode(&plaintext), Ok("4OA37Y4BXY======".to_string().into_bytes())];
/// ```
///
/// # Plaintext
///
/// Every byte stream can be a [base32](Base32Context) plaintext.
///
/// # Ciphertext
///
/// Usually base encodings are used in situations when some bytes can lead to errors while `ASCII`
/// letters and numbers are fine. As a result only byte vectors containing only bytes from the table
/// below are valid ciphertexts. All other byte vectors will be rejected by a [`Base32Context`].
///
/// [Base32](Base32Context) is case insensitive. That means, that any mixture of uppercase and
/// lowercase `ASCII` letters are accepted and can be decoded into plaintext, as long as they are
/// part of the alphabet.
///
/// # Alphabet
///
/// The following table shows the alphabet that is used by the [base32](Base32Context) code. The
/// first row and first column are the index, and the cell where a column and a row cross each other
/// is the corresponding cipher value. See the encode section below for an explanation on how the
/// ciphertext is calculated.
///
/// |         || 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | a | b | c | d | e | f |
/// |---------||---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
/// | **0x0** ||`A`|`B`|`C`|`D`|`E`|`F`|`G`|`H`|`I`|`J`|`K`|`L`|`M`|`N`|`O`|`P`|
/// | **0x1** ||`Q`|`R`|`S`|`T`|`U`|`V`|`W`|`X`|`Y`|`Z`|`2`|`3`|`4`|`5`|`6`|`7`|
/// | **0x2** ||`=`|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
///
/// # Algorithm
///
/// ## Encode
///
/// 1. The byte stream is separated into `5 bit` blocks.
/// 2. It is clear that every `5 bit` block can have4 `2^5` = `32` different values. The alphabet
///    table can be used to determine the corresponding [base32](Base32Context) value for every
///    `5 bit` block. This can be implemented as an array containing all values from the table.
///
/// ## Decode
///
/// 1. Take a character from the ciphertext and replace it with its index from the alphabet table,
///    using only `5 bits` to represent the resulting value.
/// 2. Append exactly the `5 bits` from step 1 to the plaintext.
/// 3. Repeat
///
/// All padding bytes `=` are ignored in the decode algorithm.
#[derive(Clone, Copy)]
pub struct Base32Context {
    /// The [class](AtomClass) this [`CryptographicAtom`] belongs to.
    atom_class: AtomClass,
    /// The [type](AtomType) of every instance of this [`CryptographicAtom`].
    atom_type: AtomType,
    /// The capitalization state of this instance.
    ///
    /// This variable determines if this atom's instance outputs uppercase or lowercase ciphertexts.
    ///
    /// The default value is `true`.
    is_capitalized: bool,
}

impl Base32Context {
    /// Returns the decode alphabet with only lowercase letters in a [`HashMap`].
    ///
    /// The returned [`HashMap`] maps every ciphertext value to its corresponding `5 bit` index in
    /// the [base32](Base32Context) alphabet. The padding byte `=` is mapped to `0x20`.
    fn get_lowercase_decode_alphabet() -> HashMap<u8, u64> {
        map![
            (0x61, 0), (0x62, 1), (0x63, 2), (0x64, 3),
            (0x65, 4), (0x66, 5), (0x67, 6), (0x68, 7),
            (0x69, 8), (0x6a, 9), (0x6b, 10), (0x6c, 11),
            (0x6d, 12), (0x6e, 13), (0x6f, 14), (0x70, 15),
            (0x71, 16), (0x72, 17), (0x73, 18), (0x74, 19),
            (0x75, 20), (0x76, 21), (0x77, 22), (0x78, 23),
            (0x79, 24), (0x7a, 25), (0x32, 26), (0x33, 27),
            (0x34, 28), (0x35, 29), (0x36, 30), (0x37, 31),
            (0x3d, 32)
        ]
    }

    /// Returns the decode alphabet in a [`HashMap`].
    ///
    /// The returned [`HashMap`] maps every ciphertext value to its corresponding `5 bit` index in
    /// the [base32](Base32Context) alphabet. The padding byte `=` is mapped to `0x20`.
    fn get_uppercase_decode_alphabet() -> HashMap<u8, u64> {
        map![
            (0x41, 0), (0x42, 1), (0x43, 2), (0x44, 3),
            (0x45, 4), (0x46, 5), (0x47, 6), (0x48, 7),
            (0x49, 8), (0x4a, 9), (0x4b, 10), (0x4c, 11),
            (0x4d, 12), (0x4e, 13), (0x4f, 14), (0x50, 15),
            (0x51, 16), (0x52, 17), (0x53, 18), (0x54, 19),
            (0x55, 20), (0x56, 21), (0x57, 22), (0x58, 23),
            (0x59, 24), (0x5a, 25), (0x32, 26), (0x33, 27),
            (0x34, 28), (0x35, 29), (0x36, 30), (0x37, 31),
            (0x3d, 32)
        ]
    }

    /// Returns the encode alphabet with only uppercase letters in an array.
    ///
    /// Any `5 bit` block entered into the returned array will immediately return the corresponding
    /// uppercase ciphertext value. For `0x20` the padding byte `=` will be returned.
    fn get_uppercase_encode_alphabet() -> [u8; 33] {
        [
            0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48,
            0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f, 0x50,
            0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58,
            0x59, 0x5a, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37,
            0x3d
        ]
    }

    /// Returns the encode alphabet with only lowercase letters in an array.
    ///
    /// Any `5 bit` block entered into the returned array will immediately return the corresponding
    /// lowercase ciphertext value. For `0x20` the padding byte `=` will be returned.
    fn get_lowercase_encode_alphabet() -> [u8; 33] {
        [
            0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68,
            0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f, 0x70,
            0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78,
            0x79, 0x7a, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37,
            0x3d
        ]
    }

    /// Creates a new [`Base32Context`] [`CryptographicAtom`].
    pub fn new() -> Base32Context {
        Base32Context {
            atom_class: AtomClass::Code,
            atom_type: AtomType::Base32,
            is_capitalized: true,
        }
    }

    /// Validates the given ciphertext.
    ///
    /// If the given ciphertext does not match the constraints for [`Base32Context`] ciphertexts, an
    /// [error](CryptoError) is returned. The ciphertext is validated for the capitalization mode
    /// that is set on the instance. That means that, before calling this method, the ciphertext
    /// **must** be mono cased.
    fn validate_ciphertext(
        &self,
        ciphertext: &Vec<u8>,
    ) -> Result<(), CryptoError> {
        if ciphertext.is_empty() {
            return Ok(());
        }

        if ciphertext.len() % 8 != 0 {
            return Err(CryptoError::IllegalResidueClass(
                "Failure during Base32 ciphertext validation!".to_string(),
                (ciphertext.len() % 8) as u8, 8,
            ));
        }

        let alphabet = if self.is_capitalized {
            Base32Context::get_uppercase_decode_alphabet()
        } else {
            Base32Context::get_lowercase_decode_alphabet()
        };
        let padding_byte = Base32Context::get_uppercase_encode_alphabet()[32];
        let mut i = 0;

        for c in &ciphertext[..ciphertext.len() - 6] {
            if !alphabet.contains_key(c) || *c == padding_byte {
                return Err(CryptoError::IllegalCharacter(
                    "Failure during Base32 ciphertext validation!".to_string(), i,
                    ciphertext.clone(),
                ));
            }
            i += 1;
        }

        let mut last_six_bytes_bit_string = 0;
        for c in &ciphertext[ciphertext.len() - 6..] {
            if !alphabet.contains_key(c) {
                return Err(CryptoError::IllegalCharacter(
                    "Failure during Base32 ciphertext validation!".to_string(),
                    i, ciphertext.clone(),
                ));
            }
            i += 1;
            if *c == padding_byte {
                last_six_bytes_bit_string |= (*c as u64) << 8 * (ciphertext.len() - i);
            }
        }

        if last_six_bytes_bit_string ^ ZERO_PADDING_BYTES_BIT_STRING == 0x0
            || last_six_bytes_bit_string ^ ONE_PADDING_BYTE_BIT_STRING == 0x0
            || last_six_bytes_bit_string ^ THREE_PADDING_BYTE_BIT_STRING == 0x0
            || last_six_bytes_bit_string ^ FOUR_PADDING_BYTE_BIT_STRING == 0x0
            || last_six_bytes_bit_string ^ SIX_PADDING_BYTE_BIT_STRING == 0x0 {
            Ok(())
        } else {
            Err(CryptoError::MalformedPadding(
                "Failure during Base32 ciphertext validation!".to_string(),
                ciphertext.clone(),
            ))
        }
    }
}

impl CaseInsensitiveCiphertext for Base32Context {
    /// Returns the capitalization state of this [`CryptographicAtom`].
    ///
    /// This method always returns a [`Some`] value for this [`CryptographicAtom`], because
    /// [`Base32Context`] can use uppercase and lowercase letters, but, although mixed case
    /// ciphertexts are allowed, this atom will always produce either uppercase-only or
    /// lowercase-only ciphertexts.
    fn ciphertext_is_capitalized(&self) -> Option<bool> { Some(self.is_capitalized) }

    /// Sets the ciphertext capitalization state of this [`CryptographicAtom`].
    fn set_ciphertext_capitalization(
        &mut self,
        capitalized: bool,
    ) {
        self.is_capitalized = capitalized;
    }
}

impl Code for Base32Context {}

impl CryptographicAtom for Base32Context {
    /// Returns the [class](AtomClass) the [`Base32Context`] belongs to.
    fn get_atom_class(&self) -> AtomClass { self.atom_class }

    /// Returns the [type](AtomType) of a [`Base32Context`].
    fn get_atom_type(&self) -> AtomType { self.atom_type }
}

impl Decode for Base32Context {
    /// Decodes the given ciphertext.
    ///
    /// # Errors
    ///
    /// An error is returned when
    ///
    /// * the ciphertext contains bytes that are not part of the [`base32`](Base32Context) alphabet,
    /// * the padding bytes at the end are malformed.
    fn decode(
        &mut self,
        ciphertext: &Vec<u8>,
    ) -> Result<Vec<u8>, CryptoError> {
        let mono_case_ciphertext = text_to_mono_case(
            ciphertext, self.is_capitalized,
        );
        if let Err(error) = self.validate_ciphertext(&mono_case_ciphertext) {
            Err(error)
        } else {
            Ok(base32_decode(&mono_case_ciphertext, if self.is_capitalized {
                Base32Context::get_uppercase_decode_alphabet()
            } else {
                Base32Context::get_lowercase_decode_alphabet()
            }))
        }
    }
}

impl Encode for Base32Context {
    /// Encodes the given plaintext.
    fn encode(
        &mut self,
        plaintext: &Vec<u8>,
    ) -> Result<Vec<u8>, CryptoError> {
        if self.is_capitalized {
            Ok(base32_encode(plaintext, Base32Context::get_uppercase_encode_alphabet()))
        } else {
            Ok(base32_encode(plaintext, Base32Context::get_lowercase_encode_alphabet()))
        }
    }
}



/// Test vectors for the [base32](Base32Context) code.
///
/// Some of the tests have been taken from `RFC 4648`. Additional tests have been added to increase
/// data coverage, especially edge cases.
#[cfg(any(feature = "doc_tests", test))]
mod tests {
    use super::*;

    /// Tests the [`get_atom_class`](Base32Context::get_atom_class) method of the [`Base32Context`]
    /// struct
    ///
    /// This method **must** return [`Code`](AtomClass::Code).
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_01() {
        let ctx = Base32Context::new();
        assert_eq![ctx.get_atom_class(), AtomClass::Code];
    }

    /// Tests the [`get_atom_type`](Base32Context::get_atom_type) method of the [`Base32Context`]
    /// struct.
    ///
    /// This method **must** return [`Base32`](AtomType::Base32).
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_02() {
        let ctx = Base32Context::new();
        assert_eq![ctx.get_atom_type(), AtomType::Base32];
    }

    /// Tests the [`ciphertext_is_capitalized`](Base32Context::ciphertext_is_capitalized) method.
    ///
    /// The default capitalization after the creation of a new instance of [`Base32Context`]
    /// **must** be `true`.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_03() {
        let ctx = Base32Context::new();
        assert_eq![ctx.ciphertext_is_capitalized(), Some(true)];
    }

    /// Tests the [`set_ciphertext_capitalization`](Base32Context::set_ciphertext_capitalization)
    /// method.
    ///
    /// Tests if the capitalization state is `true` after setting it to `true`.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_04() {
        let mut ctx = Base32Context::new();
        ctx.set_ciphertext_capitalization(true);
        assert_eq![ctx.ciphertext_is_capitalized(), Some(true)];
    }

    /// Tests the [`set_ciphertext_capitalization`](Base32Context::set_ciphertext_capitalization)
    /// method.
    ///
    /// Tests if the capitalization state is `true` after setting it to `false` and then to `true`
    /// again.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_05() {
        let mut ctx = Base32Context::new();
        ctx.set_ciphertext_capitalization(false);
        ctx.set_ciphertext_capitalization(true);
        assert_eq![ctx.ciphertext_is_capitalized(), Some(true)];
    }

    /// Tests the [`set_ciphertext_capitalization`](Base32Context::set_ciphertext_capitalization)
    /// method.
    ///
    /// Tests if the capitalization state is `false` after setting it to `false`.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_06() {
        let mut ctx = Base32Context::new();
        ctx.set_ciphertext_capitalization(false);
        assert_eq![ctx.ciphertext_is_capitalized(), Some(false)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base32Context::decode) method.
    ///
    /// Ciphertext: `B32*`
    ///
    /// Plaintext: `<empty byte array>`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_07() {
        let ciphertext = vec![];
        let expected_plaintext = vec![];

        let mut ctx = Base32Context::new();
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base32Context::decode) method.
    ///
    /// Ciphertext: `B32*MY= === ==`
    ///
    /// Plaintext: `f`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_08() {
        let ciphertext = vec![
            0x4d, 0x59, 0x3d, 0x3d,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];
        let expected_plaintext = vec![0x66];

        let mut ctx = Base32Context::new();
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base32Context::decode) method.
    ///
    /// Ciphertext: `B32*mzx q== ==`
    ///
    /// Plaintext: `fo`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_09() {
        let ciphertext = vec![
            0x6d, 0x7a, 0x78, 0x71,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];
        let expected_plaintext = vec![0x66, 0x6f];

        let mut ctx = Base32Context::new();
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base32Context::decode) method.
    ///
    /// Ciphertext: `B32*MZX W6= ==`
    ///
    /// Plaintext: `foo`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_10() {
        let ciphertext = vec![
            0x4d, 0x5a, 0x58, 0x57,
            0x36, 0x3d, 0x3d, 0x3d,
        ];
        let expected_plaintext = vec![0x66, 0x6f, 0x6f];

        let mut ctx = Base32Context::new();
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base32Context::decode) method.
    ///
    /// Ciphertext: `B32*Mzx W6y Q=`
    ///
    /// Plaintext: `foob`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_11() {
        let ciphertext = vec![
            0x4d, 0x7a, 0x78, 0x57,
            0x36, 0x79, 0x51, 0x3d,
        ];
        let expected_plaintext = vec![0x66, 0x6f, 0x6f, 0x62];

        let mut ctx = Base32Context::new();
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base32Context::decode) method.
    ///
    /// Ciphertext: `B32*mzx w6y tb`
    ///
    /// Plaintext: `fooba`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_12() {
        let ciphertext = vec![
            0x6d, 0x7a, 0x78, 0x77,
            0x36, 0x79, 0x74, 0x62,
        ];
        let expected_plaintext = vec![0x66, 0x6f, 0x6f, 0x62, 0x61];

        let mut ctx = Base32Context::new();
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base32Context::decode) method.
    ///
    /// Ciphertext: `B32*mZX w6Y Tbo i== === =`
    ///
    /// Plaintext: `foobar`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_13() {
        let ciphertext = vec![
            0x6d, 0x5a, 0x58, 0x77,
            0x36, 0x59, 0x54, 0x62,
            0x6f, 0x69, 0x3d, 0x3d,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];
        let expected_plaintext = vec![0x66, 0x6f, 0x6f, 0x62, 0x61, 0x72];

        let mut ctx = Base32Context::new();
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Additional test covering additional characters of the alphabet for the
    /// [`decode`](Base32Context::decode) method.
    ///
    /// Ciphertext: `B32*4oa 37Y 4bX y== === =`
    ///
    /// Plaintext: `みま`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_14() {
        let ciphertext = vec![
            0x34, 0x6f, 0x61, 0x33,
            0x37, 0x59, 0x34, 0x62,
            0x58, 0x79, 0x3d, 0x3d,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];
        let expected_plaintext = vec![0xe3, 0x81, 0xbf, 0xe3, 0x81, 0xbe];

        let mut ctx = Base32Context::new();
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`encode`](Base32Context::encode) method.
    ///
    /// Plaintext: `<empty byte array>`
    ///
    /// Ciphertext: `B32*`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_15() {
        let plaintext = vec![];
        let expected_ciphertext = vec![];

        let mut ctx = Base32Context::new();
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Test vector from `RFC 4648` for the [`encode`](Base32Context::encode) method.
    ///
    /// Plaintext: `f`
    ///
    /// Ciphertext: `B32*MY= === ==`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_16() {
        let plaintext = vec![0x66];
        let expected_ciphertext = vec![
            0x4d, 0x59, 0x3d, 0x3d,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];

        let mut ctx = Base32Context::new();
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Test vector from `RFC 4648` for the [`encode`](Base32Context::encode) method.
    ///
    /// Plaintext: `fo`
    ///
    /// Ciphertext: `B32*mzx q== ==`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_17() {
        let plaintext = vec![0x66, 0x6f];
        let expected_ciphertext = vec![
            0x6d, 0x7a, 0x78, 0x71,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];

        let mut ctx = Base32Context::new();
        ctx.is_capitalized = false;
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Test vector from `RFC 4648` for the [`encode`](Base32Context::encode) method.
    ///
    /// Plaintext: `foo`
    ///
    /// Ciphertext: `B32*MZX W6= ==`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_18() {
        let plaintext = vec![0x66, 0x6f, 0x6f];
        let expected_ciphertext = vec![
            0x4d, 0x5a, 0x58, 0x57,
            0x36, 0x3d, 0x3d, 0x3d,
        ];

        let mut ctx = Base32Context::new();
        ctx.is_capitalized = true;
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Test vector from `RFC 4648` for the [`encode`](Base32Context::encode) method.
    ///
    /// Plaintext: `foob`
    ///
    /// Ciphertext: `B32*mzx w6y q=`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_19() {
        let plaintext = vec![0x66, 0x6f, 0x6f, 0x62];
        let expected_ciphertext = vec![
            0x6d, 0x7a, 0x78, 0x77,
            0x36, 0x79, 0x71, 0x3d,
        ];

        let mut ctx = Base32Context::new();
        ctx.is_capitalized = false;
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Test vector from `RFC 4648` for the [`encode`](Base32Context::encode) method.
    ///
    /// Plaintext: `fooba`
    ///
    /// Ciphertext: `B32*MZX W6Y TB`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_20() {
        let plaintext = vec![0x66, 0x6f, 0x6f, 0x62, 0x61];
        let expected_ciphertext = vec![
            0x4d, 0x5a, 0x58, 0x57,
            0x36, 0x59, 0x54, 0x42,
        ];

        let mut ctx = Base32Context::new();
        ctx.is_capitalized = true;
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Test vector from `RFC 4648` for the [`encode`](Base32Context::encode) method.
    ///
    /// Plaintext: `foobar`
    ///
    /// Ciphertext: `B32*MZX W6Y TBO I== === =`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_21() {
        let plaintext = vec![0x66, 0x6f, 0x6f, 0x62, 0x61, 0x72];
        let expected_ciphertext = vec![
            0x4d, 0x5a, 0x58, 0x57,
            0x36, 0x59, 0x54, 0x42,
            0x4f, 0x49, 0x3d, 0x3d,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];

        let mut ctx = Base32Context::new();
        ctx.is_capitalized = true;
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Additional test for the [`encode`](Base32Context::encode) method.
    ///
    /// Plaintext = `みま`
    ///
    /// ```
    /// //                 みま = 0x        3      0    7       f         3      0    7       e
    /// //       (into binary) = 0b        0011   00 0001   11 1111      0011   00'0001   11 1110
    /// //        (into UTF-8) = 0b   1110'0011 1000'0001 1011'1111 1110'0011 1000'0001 1011'1110
    /// //          (into hex) = 0x   e3        81        bf        e3        81        be
    /// // (add padding bytes) = 0b   1110 0011 1000 0001 1011 1111 1110 0011 1000 0001
    /// //                            1011 1110 0000 0000 0000 0000 0000 0000 0000 0000
    /// //     (separate into
    /// //       5-bit blocks) = 0b   1'1100 0'1110 0'0000 1'1011 1'1111 1'1000 1'1100 0'0001
    /// //                            1'0111 1'1000 0'0000 0'0000 0'0000 0'0000 0'0000 0'0000
    /// //          (into hex) = 0x   1c     0e     00     1b     1f     18     1c     01
    /// //                            17     18     20     20     20     20     20     20
    /// //          (Base32()) = B32* 4      O      A      3      7      Y      4      B
    /// //                            X      Y      =      =      =      =      =      =
    /// ```
    ///
    /// Ciphertext = `B32*4oa 37y 4bx y== === =`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_22() {
        let plaintext = vec![0xe3, 0x81, 0xbf, 0xe3, 0x81, 0xbe];
        let expected_ciphertext = vec![
            0x34, 0x6f, 0x61, 0x33,
            0x37, 0x79, 0x34, 0x62,
            0x78, 0x79, 0x3d, 0x3d,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];

        let mut ctx = Base32Context::new();
        ctx.is_capitalized = false;
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Tests the [`Base32Context`]
    /// [`decode_lowercase_alphabet`](Base32Context::get_lowercase_decode_alphabet) associated
    /// function.
    ///
    /// This test is meant as a friendly reminder, if the [`Base32Context`] code will ever be
    /// changed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_23() {
        let mut decode_alphabet: HashMap<u8, u64> = HashMap::new();

        let mut k = 0x60;
        for v in 0..=25 {
            k += 1;
            decode_alphabet.insert(k, v);
        }

        k = 0x31;
        for v in 26..=31 {
            k += 1;
            decode_alphabet.insert(k, v);
        }

        decode_alphabet.insert(0x3d, 32);

        assert_eq![Base32Context::get_lowercase_decode_alphabet().len(), decode_alphabet.len()];
        assert_eq![Base32Context::get_lowercase_decode_alphabet(), decode_alphabet];
    }

    /// Tests the [`Base32Context`]
    /// [`decode_uppercase_alphabet`](Base32Context::get_uppercase_decode_alphabet) associated
    /// function.
    ///
    /// This test is meant as a friendly reminder, if the [`Base32Context`] code will ever be
    /// changed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_24() {
        let mut decode_alphabet: HashMap<u8, u64> = HashMap::new();

        let mut k = 0x40;
        for v in 0..=25 {
            k += 1;
            decode_alphabet.insert(k, v);
        }

        k = 0x31;
        for v in 26..=31 {
            k += 1;
            decode_alphabet.insert(k, v);
        }

        decode_alphabet.insert(0x3d, 32);

        assert_eq![Base32Context::get_uppercase_decode_alphabet().len(), decode_alphabet.len()];
        assert_eq![Base32Context::get_uppercase_decode_alphabet(), decode_alphabet];
    }

    /// Tests the [`Base32Context`]
    /// [`get_uppercase_encode_alphabet`](Base32Context::get_uppercase_encode_alphabet) associated
    /// function.
    ///
    /// This test is meant as a friendly reminder, if the [`Base32Context`] code will ever be
    /// changed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_25() {
        let mut encode_alphabet: [u8; 33] = [0; 33];

        let mut k = 0;
        for v in 0x41..=0x5a {
            encode_alphabet[k] = v;
            k += 1;
        }
        for v in 0x32..=0x37 {
            encode_alphabet[k] = v;
            k += 1;
        }
        encode_alphabet[k] = 0x3d;

        assert_eq![Base32Context::get_uppercase_encode_alphabet().len(), encode_alphabet.len()];
        assert_eq![Base32Context::get_uppercase_encode_alphabet(), encode_alphabet];
    }

    /// Tests the [`Base32Context`]
    /// [`get_lowercase_encode_alphabet`](Base32Context::get_lowercase_encode_alphabet) associated
    /// function.
    ///
    /// This test is meant as a friendly reminder, if the [`Base32Context`] code will ever be
    /// changed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_26() {
        let mut encode_alphabet: [u8; 33] = [0; 33];

        let mut k = 0;
        for v in 0x61..=0x7a {
            encode_alphabet[k] = v;
            k += 1;
        }
        for v in 0x32..=0x37 {
            encode_alphabet[k] = v;
            k += 1;
        }
        encode_alphabet[k] = 0x3d;

        assert_eq![Base32Context::get_lowercase_decode_alphabet().len(), encode_alphabet.len()];
        assert_eq![Base32Context::get_lowercase_encode_alphabet(), encode_alphabet];
    }

    /// Tests the [`validate_ciphertext`](Base32Context::validate_ciphertext) method.
    ///
    /// Validating an empty string ciphertext **must** succeed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_27() {
        let ciphertext = vec![];
        let mut ctx = Base32Context::new();
        ctx.is_capitalized = true;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Ok(())];
    }

    /// Tests the [`validate_ciphertext`](Base32Context::validate_ciphertext) method.
    ///
    /// Validating a [`Base32Context`] ciphertext that contains no padding bytes `=` **must**
    /// succeed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_28() {
        let ciphertext = vec![
            0x41, 0x42, 0x43, 0x44,
            0x45, 0x46, 0x47, 0x48,
        ];
        let mut ctx = Base32Context::new();
        ctx.is_capitalized = true;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Ok(())];
    }

    /// Tests the [`validate_ciphertext`](Base32Context::validate_ciphertext) method.
    ///
    /// Validating a [`Base32Context`] ciphertext that contains exactly one padding byte `=`
    /// **must** succeed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_29() {
        let ciphertext = vec![
            0x69, 0x6a, 0x6b, 0x6c,
            0x6d, 0x6e, 0x6f, 0x3d,
        ];
        let mut ctx = Base32Context::new();
        ctx.is_capitalized = false;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Ok(())];
    }

    /// Tests the [`validate_ciphertext`](Base32Context::validate_ciphertext) method.
    ///
    /// Validating a [`Base32Context`] ciphertext with exactly three padding bytes `=` **must**
    /// succeed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_30() {
        let ciphertext = vec![
            0x70, 0x71, 0x72, 0x73,
            0x74, 0x3d, 0x3d, 0x3d,
        ];
        let mut ctx = Base32Context::new();
        ctx.is_capitalized = false;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Ok(())];
    }

    /// Tests the [`validate_ciphertext`](Base32Context::validate_ciphertext) method.
    ///
    /// Validating a [`Base32Context`] ciphertext with exactly four padding bytes `=` **must**
    /// succeed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_31() {
        let ciphertext = vec![
            0x55, 0x56, 0x57, 0x58,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];
        let mut ctx = Base32Context::new();
        ctx.is_capitalized = true;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Ok(())];
    }

    /// Tests the [`validate_ciphertext`](Base32Context::validate_ciphertext) method.
    ///
    /// Validating a [`Base32Context`] ciphertext with exactly six padding bytes `=` **must**
    /// succeed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_32() {
        let ciphertext = vec![
            0x79, 0x7a, 0x3d, 0x3d,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];
        let mut ctx = Base32Context::new();
        ctx.is_capitalized = false;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Ok(())];
    }

    /// Tests the [`validate_ciphertext`](Base32Context::validate_ciphertext) method.
    ///
    /// Validating a [`Base32Context`] ciphertext that has a wrong residue class **must** fail.
    ///
    /// Only one test is required because all wrong residue class errors are handled by the same
    /// branch in the [`validate_ciphertext`](Base32Context::validate_ciphertext) function.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_33() {
        let ciphertext = vec![
            0x32, 0x33, 0x34, 0x35,
            0x36, 0x36, 0x37,
        ];
        let ctx = Base32Context::new();
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::IllegalResidueClass(
            "Failure during Base32 ciphertext validation!".to_string(), 7, 8,
        ))];
    }

    /// Tests the [`validate_ciphertext`](Base32Context::validate_ciphertext) method.
    ///
    /// Validating a [`Base32Context`] ciphertext that contains a character that is not part of the
    /// [`Base32Context`] alphabet and is not one of the last six bytes of the ciphertext **must**
    /// fail.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_34() {
        let ciphertext = vec![
            0x61, 0x62, 0x63, 0x64,
            0xff, 0x46, 0x47, 0x48,
            0x49, 0x4a, 0x4b, 0x4c,
            0x4d, 0x4e, 0x4f, 0x50,
        ];
        let mut ctx = Base32Context::new();
        ctx.is_capitalized = false;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::IllegalCharacter(
            "Failure during Base32 ciphertext validation!".to_string(), 4,
            ciphertext,
        ))];
    }

    /// Tests the [`validate_ciphertext`](Base32Context::validate_ciphertext) method.
    ///
    /// Validating a [`Base32Context`] ciphertext that contains a padding byte that appears before
    /// the sixth last position of the ciphertext **must** fail.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_35() {
        let ciphertext = vec![
            0x71, 0x72, 0x73, 0x74,
            0x75, 0x76, 0x77, 0x3d,
            0x3d, 0x3d, 0x3d, 0x3d,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];
        let mut ctx = Base32Context::new();
        ctx.is_capitalized = false;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::IllegalCharacter(
            "Failure during Base32 ciphertext validation!".to_string(),
            7, ciphertext,
        ))];
    }

    /// Tests the [`validate_ciphertext`](Base32Context::validate_ciphertext) method.
    ///
    /// Validating a [`Base32Context`] ciphertext that contains a character that is not part of the
    /// [`Base32Context`] alphabet and is among the last six bytes of the ciphertext **must** fail.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_36() {
        let mut ciphertext = vec![
            0x59, 0x5a, 0x32, 0x33,
            0x34, 0x35, 0x36, 0x00,
        ];
        let mut ctx = Base32Context::new();
        ctx.is_capitalized = true;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::IllegalCharacter(
            "Failure during Base32 ciphertext validation!".to_string(), 7,
            ciphertext.clone(),
        ))];
        let mut c = 0x38;
        for i in (3..=7).rev() {
            c -= 1;
            ciphertext[i] = c;
            ciphertext[i - 1] = 0x00;
            assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::IllegalCharacter(
                "Failure during Base32 ciphertext validation!".to_string(), i - 1,
                ciphertext.clone(),
            ))];
        }
    }

    /// Tests the [`validate_ciphertext`](Base32Context::validate_ciphertext) method.
    ///
    /// Validating a [`Base32Context`] ciphertext with malformed padding bytes **must** fail. This
    /// test uses a [`Base32Context`] ciphertext with exactly five padding bytes `=` on the last
    /// five positions of the ciphertext.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_37() {
        let ciphertext = vec![
            0x41, 0x42, 0x43, 0x3d,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];
        let mut ctx = Base32Context::new();
        ctx.is_capitalized = true;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::MalformedPadding(
            "Failure during Base32 ciphertext validation!".to_string(), ciphertext,
        ))];
    }

    /// Tests the [`validate_ciphertext`](Base32Context::validate_ciphertext) method.
    ///
    /// Validating a [`Base32Context`] ciphertext with malformed padding bytes **must** fail. This
    /// test uses a [`Base32Context`] ciphertext with exactly two padding bytes `=` on the last two
    /// positions of the ciphertext.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_38() {
        let ciphertext = vec![
            0x65, 0x66, 0x67, 0x68,
            0x69, 0x6a, 0x3d, 0x3d,
        ];
        let mut ctx = Base32Context::new();
        ctx.is_capitalized = false;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::MalformedPadding(
            "Failure during Base32 ciphertext validation!".to_string(), ciphertext,
        ))];
    }

    /// Tests the [`validate_ciphertext`](Base32Context::validate_ciphertext) method.
    ///
    /// Validating a [`Base32Context`] ciphertext with malformed padding bytes **must** fail. This
    /// test uses a [`Base32Context`] ciphertext with one padding byte on the sixth last and one
    /// padding byte on the last position of the ciphertext.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_39() {
        let ciphertext = vec![
            0x69, 0x6a, 0x3d, 0x6c,
            0x6d, 0x6e, 0x6f, 0x3d,
        ];
        let mut ctx = Base32Context::new();
        ctx.is_capitalized = false;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::MalformedPadding(
            "Failure during Base32 ciphertext validation!".to_string(), ciphertext,
        ))];
    }

    /// Tests the [`validate_ciphertext`](Base32Context::validate_ciphertext) method.
    ///
    /// Validating a [`Base32Context`] ciphertext with malformed padding bytes **must** fail. This
    /// test uses a [`Base32Context`] ciphertext with two padding bytes on the sixth last and fifth
    /// last position and one padding byte on the second last position of the ciphertext.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_40() {
        let ciphertext = vec![
            0x6d, 0x6e, 0x3d, 0x3d,
            0x71, 0x72, 0x3d, 0x74,
        ];
        let mut ctx = Base32Context::new();
        ctx.is_capitalized = false;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::MalformedPadding(
            "Failure during Base32 ciphertext validation!".to_string(), ciphertext,
        ))];
    }

    /// Tests the [`validate_ciphertext`](Base32Context::validate_ciphertext) method.
    ///
    /// Validating a [`Base32Context`] ciphertext with malformed padding bytes **must** fail. This
    /// test uses a [`Base32Context`] ciphertext five padding bytes on the sixth last to the second
    /// last position of the ciphertext.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_41() {
        let ciphertext = vec![
            0x55, 0x56, 0x3d, 0x3d,
            0x3d, 0x3d, 0x3d, 0x33,
        ];
        let mut ctx = Base32Context::new();
        ctx.is_capitalized = true;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::MalformedPadding(
            "Failure during Base32 ciphertext validation!".to_string(),
            ciphertext,
        ))];
    }

    /// Tests the [`validate_ciphertext`](Base32Context::validate_ciphertext) method.
    ///
    /// Validating a [`Base32Context`] ciphertext with malformed padding bytes **must** fail. This
    /// test uses a [`Base32Context`] ciphertext one padding byte on the third last position of the
    /// ciphertext.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_42() {
        let ciphertext = vec![
            0x34, 0x35, 0x36, 0x37,
            0x61, 0x3d, 0x63, 0x64,
        ];
        let mut ctx = Base32Context::new();
        ctx.is_capitalized = false;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::MalformedPadding(
            "Failure during Base32 ciphertext validation!".to_string(), ciphertext,
        ))];
    }
}

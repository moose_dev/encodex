// Copyright (C) 2023  Fabian Moos
// This file is part of encodex.
//
// encodex is free software: you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// encodex is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
// even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with encodex. If not,
// see <https://www.gnu.org/licenses/>.
//

//! Functionality for handling [base16](`Base16Context`) codes.

use std::collections::hash_map::HashMap;

use crate::{
    AtomClass,
    AtomType,
    CaseInsensitiveCiphertext,
    code::{
        Code,
        Decode,
        Encode,
    },
    CryptoError,
    CryptographicAtom,
    map,
    util::text_to_mono_case,
};



/// [`CryptographicAtom`] for handling [base16](Base16Context) plain- and ciphertexts.
///
/// The prefix `B16*` is used for [base16](Base16Context) strings in documentation comments.
///
/// # Usage Example
///
/// ```
/// use encodex::code::base_encoding::Base16Context;
/// use encodex::code::Decode;
///
/// let mut ctx = Base16Context::new();
/// let ciphertext = "5468697320656e636f64696e6720646f75626c6573\
///                   20746865206d656d6f727920636f6e73756d707469\
///                   6f6e206f662061206279746520766563746f722120\
///                   5361646765203a28".to_string().into_bytes();
///
/// assert_eq![ctx.decode(&ciphertext), Ok("This encoding doubles the memory \
///                                         consumption of a byte vector! \
///                                         Sadge :(".to_string().into_bytes())];
/// ```
///
/// # Plaintext
///
/// Every byte stream can be a [base16](Base16Context) plaintext.
///
/// # Ciphertext
///
/// Usually base encodings are used in situations when some bytes can lead to errors while `ASCII`
/// letters and numbers are fine. As a result only byte vectors containing only bytes from the table
/// below are valid ciphertexts. All other byte vectors will be rejected by a [`Base16Context`].
///
/// [Base16](Base16Context) is case insensitive. That means, that any mixture of uppercase and
/// lowercase `ASCII` letters are accepted and can be decoded into plaintext, as long as they are
/// part of the alphabet.
///
/// # Alphabet
///
/// The following table shows the alphabet that is used by the [base16](Base16Context) code. The
/// first row and first column are the index, and the cell where a column and a row cross each other
/// is the corresponding cipher value. See the encode section below for an explanation on how the
/// ciphertext is calculated.
///
/// |         || 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | a | b | c | d | e | f |
/// |---------||---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
/// | **0x0** ||`0`|`1`|`2`|`3`|`4`|`5`|`6`|`7`|`8`|`9`|`A`|`B`|`C`|`D`|`E`|`F`|
///
/// Since all byte streams can be seperated into `4 bit` blocks without rest, there is no padding
/// byte required for [base16](Base16Context).
///
/// # Algorithm
///
/// ## Encode
///
/// 1. The byte stream is separated into `4 bit` blocks.
/// 2. It is clear that every `4 bit` block can have4 `2^4` = `16` different values. The alphabet
///    table can be used to determine the corresponding [base16](Base16Context) value for every
///    `4 bit` block. This can be implemented as an array containing all values from the table.
///
/// ## Decode
///
/// 1. Take a character from the ciphertext and replace it with its index from the alphabet table,
///    using only `4 bits` to represent the resulting value.
/// 2. Append exactly the `4 bits` from step 1 to the plaintext.
/// 3. Repeat
#[derive(Clone, Copy)]
pub struct Base16Context {
    /// The [class](AtomClass) this [`CryptographicAtom`] belongs to.
    atom_class: AtomClass,
    /// The [type](AtomType) of every instance of this [`CryptographicAtom`].
    atom_type: AtomType,
    /// The capitalization state of this instance.
    ///
    /// This variable determines if this atom's instance outputs uppercase or lowercase ciphertexts.
    ///
    /// The default value is `false`.
    is_capitalized: bool,
}

impl Base16Context {
    /// Returns the decode alphabet in a [`HashMap`].
    ///
    /// The returned [`HashMap`] maps every lowercase ciphertext value to its corresponding `4 bit`
    /// index in the alphabet.
    fn get_lowercase_decode_alphabet() -> HashMap<u8, u8> {
        map![
            (0x30, 0), (0x31, 1), (0x32, 2), (0x33, 3),
            (0x34, 4), (0x35, 5), (0x36, 6), (0x37, 7),
            (0x38, 8), (0x39, 9), (0x61, 10), (0x62, 11),
            (0x63, 12), (0x64, 13), (0x65, 14), (0x66, 15)
        ]
    }

    /// Returns the decode alphabet in a [`HashMap`].
    ///
    /// The returned [`HashMap`] maps every uppercase ciphertext value to its corresponding `4 bit`
    /// index in the alphabet.
    fn get_uppercase_decode_alphabet() -> HashMap<u8, u8> {
        map![
            (0x30, 0), (0x31, 1), (0x32, 2), (0x33, 3),
            (0x34, 4), (0x35, 5), (0x36, 6), (0x37, 7),
            (0x38, 8), (0x39, 9), (0x41, 10), (0x42, 11),
            (0x43, 12), (0x44, 13), (0x45, 14), (0x46, 15)
        ]
    }

    /// Returns the encode alphabet with only lowercase letters in an array.
    ///
    /// Any `4 bit` block entered into the returned array will immediately return the corresponding
    /// uppercase ciphertext value.
    fn get_lowercase_encode_alphabet() -> [u8; 16] {
        [
            0x30, 0x31, 0x32, 0x33,
            0x34, 0x35, 0x36, 0x37,
            0x38, 0x39, 0x61, 0x62,
            0x63, 0x64, 0x65, 0x66,
        ]
    }

    /// Returns the encode alphabet with only uppercase letters in an array.
    ///
    /// Any `4 bit` block entered into the returned array will immediately return the corresponding
    /// uppercase ciphertext value.
    fn get_uppercase_encode_alphabet() -> [u8; 16] {
        [
            0x30, 0x31, 0x32, 0x33,
            0x34, 0x35, 0x36, 0x37,
            0x38, 0x39, 0x41, 0x42,
            0x43, 0x44, 0x45, 0x46,
        ]
    }

    /// Creates a new [`Base16Context`] [`CryptographicAtom`] with default values.
    pub fn new() -> Base16Context {
        Base16Context {
            atom_class: AtomClass::Code,
            atom_type: AtomType::Base16,
            is_capitalized: false,
        }
    }

    /// Validates the given ciphertext.
    ///
    /// If the given ciphertext does not match the constraints for [`Base16Context`] ciphertexts, an
    /// [error](CryptoError) is returned. The ciphertext is validated for the capitalization mode
    /// that is set on the instance. That means that, before calling this method, the ciphertext
    /// **must** be mono cased.
    fn validate_ciphertext(
        &self,
        ciphertext: &Vec<u8>,
    ) -> Result<(), CryptoError> {
        if ciphertext.len() % 2 != 0 {
            return Err(CryptoError::IllegalResidueClass(
                "Failure during Base16 ciphertext validation!".to_string(), 1, 2,
            ));
        }

        let alphabet = if self.is_capitalized {
            Base16Context::get_uppercase_decode_alphabet()
        } else {
            Base16Context::get_lowercase_decode_alphabet()
        };
        let mut i = 0;

        for c in ciphertext {
            if !alphabet.contains_key(c) {
                return Err(CryptoError::IllegalCharacter(
                    "Failure during Base16 ciphertext validation!".to_string(), i,
                    ciphertext.clone(),
                ));
            }
            i += 1;
        }

        Ok(())
    }
}

impl CaseInsensitiveCiphertext for Base16Context {
    /// Returns the capitalization state of this [`CryptographicAtom`].
    ///
    /// This method always returns a [`Some`] value for this [`CryptographicAtom`], because a
    /// [`Base16Context`] can use uppercase and lowercase letters, but, although mixed case
    /// ciphertexts are allowed, this atom will always produce either uppercase-only or
    /// lowercase-only ciphertexts.
    fn ciphertext_is_capitalized(&self) -> Option<bool> { Some(self.is_capitalized) }

    /// Sets the ciphertext capitalization state of this [`CryptographicAtom`].
    fn set_ciphertext_capitalization(
        &mut self,
        capitalized: bool,
    ) {
        self.is_capitalized = capitalized;
    }
}

impl Code for Base16Context {}

impl CryptographicAtom for Base16Context {
    /// Returns the [class](AtomClass) the [`Base16Context`] belongs to.
    fn get_atom_class(&self) -> AtomClass { self.atom_class }

    /// Returns the [type](AtomType) of a [`Base16Context`].
    fn get_atom_type(&self) -> AtomType { self.atom_type }
}

impl Decode for Base16Context {
    /// Decodes the given ciphertext.
    ///
    /// # Errors
    ///
    /// An error is returned when the ciphertext contains bytes that are not part of the
    /// [`base16`](Base16Context) alphabet.
    fn decode(
        &mut self,
        ciphertext: &Vec<u8>,
    ) -> Result<Vec<u8>, CryptoError> {
        let mono_case_ciphertext = text_to_mono_case(
            ciphertext, self.is_capitalized,
        );
        if let Err(error) = self.validate_ciphertext(&mono_case_ciphertext) {
            Err(error)
        } else {
            let mut plaintext = Vec::new();
            let mut iter = mono_case_ciphertext.iter();
            let mut maybe_next = iter.next();
            let alphabet = if self.is_capitalized {
                Base16Context::get_uppercase_decode_alphabet()
            } else {
                Base16Context::get_lowercase_decode_alphabet()
            };

            while maybe_next != None {
                let mut block: u8 = 0;

                // Get first character of block.
                let mut byte = maybe_next.unwrap();
                let mut character = *alphabet.get(byte).unwrap();
                block |= character << 4;

                // Get second character of block.
                byte = iter.next().unwrap();
                character = *alphabet.get(byte).unwrap();
                block |= character;

                plaintext.push(block);
                maybe_next = iter.next();
            }

            Ok(plaintext)
        }
    }
}

impl Encode for Base16Context {
    /// Encodes the given plaintext.
    fn encode(
        &mut self,
        plaintext: &Vec<u8>,
    ) -> Result<Vec<u8>, CryptoError> {
        let mut ciphertext = Vec::new();
        let mut iter = plaintext.iter();
        let mut byte = iter.next();
        let alphabet = if self.is_capitalized {
            Base16Context::get_uppercase_encode_alphabet()
        } else {
            Base16Context::get_lowercase_encode_alphabet()
        };

        while byte != None {
            let value = byte.unwrap();
            ciphertext.push(alphabet[(value >> 4) as usize]);
            ciphertext.push(alphabet[(value & 0xf) as usize]);
            byte = iter.next();
        }

        Ok(ciphertext)
    }
}



/// Test vectors for the [base16](Base16Context) code.
///
/// Some of the tests have been taken from `RFC 4648`. Additional tests have been added to increase
/// data coverage, especially edge cases.
#[cfg(any(feature = "doc_tests", test))]
mod tests {
    use super::*;

    /// Tests the [`get_atom_class`](Base16Context::get_atom_class) method of the [`Base16Context`]
    /// struct.
    ///
    /// This method **must** return [`Code`](AtomClass::Code).
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_01() {
        let ctx = Base16Context::new();
        assert_eq![ctx.get_atom_class(), AtomClass::Code];
    }

    /// Tests the [`get_atom_type`](Base16Context::get_atom_type) method of the [`Base16Context`]
    /// struct.
    ///
    /// This method **must** return [`Base16`](AtomType::Base16).
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_02() {
        let ctx = Base16Context::new();
        assert_eq![ctx.get_atom_type(), AtomType::Base16];
    }

    /// Tests the [`ciphertext_is_capitalized`](Base16Context::ciphertext_is_capitalized) method.
    ///
    /// The default capitalization after the creation of a new instance of [`Base16Context`]
    /// **must** be `false`.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_03() {
        let ctx = Base16Context::new();
        assert_eq![ctx.ciphertext_is_capitalized(), Some(false)];
    }

    /// Tests the [`set_ciphertext_capitalization`](Base16Context::set_ciphertext_capitalization)
    /// method.
    ///
    /// Tests if the capitalization state is `true` after setting it to `true`.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_04() {
        let mut ctx = Base16Context::new();
        ctx.set_ciphertext_capitalization(true);
        assert_eq![ctx.ciphertext_is_capitalized(), Some(true)];
    }

    /// Tests the [`set_ciphertext_capitalization`](Base16Context::set_ciphertext_capitalization)
    /// method.
    ///
    /// Tests if the capitalization state is `false` after setting it to `true` and then to `false`
    /// again.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_05() {
        let mut ctx = Base16Context::new();
        ctx.set_ciphertext_capitalization(true);
        ctx.set_ciphertext_capitalization(false);
        assert_eq![ctx.ciphertext_is_capitalized(), Some(false)];
    }

    /// Tests the [`set_ciphertext_capitalization`](Base16Context::set_ciphertext_capitalization)
    /// method.
    ///
    /// Tests if the capitalization state is `false` after setting it to `false`.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_06() {
        let mut ctx = Base16Context::new();
        ctx.set_ciphertext_capitalization(false);
        assert_eq![ctx.ciphertext_is_capitalized(), Some(false)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base16Context::decode) method.
    ///
    /// Ciphertext: `B16*`
    ///
    /// Plaintext: `<empty byte array>`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_07() {
        let ciphertext = vec![];
        let expected_plaintext = vec![];

        let mut ctx = Base16Context::new();
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base16Context::decode) method.
    ///
    /// Ciphertext: `B16*66`
    ///
    /// Plaintext: `f`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_08() {
        let ciphertext = vec![
            0x36, 0x36,
        ];
        let expected_plaintext = vec![0x66];

        let mut ctx = Base16Context::new();
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base16Context::decode) method.
    ///
    /// Ciphertext: `B16*66 6f`
    ///
    /// Plaintext: `fo`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_09() {
        let ciphertext = vec![
            0x36, 0x36, 0x36, 0x66,
        ];
        let expected_plaintext = vec![0x66, 0x6f];

        let mut ctx = Base16Context::new();
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base16Context::decode) method.
    ///
    /// Ciphertext: `B16*66 6F 6F`
    ///
    /// Plaintext: `foo`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_10() {
        let ciphertext = vec![
            0x36, 0x36, 0x36, 0x46,
            0x36, 0x46,
        ];
        let expected_plaintext = vec![0x66, 0x6f, 0x6f];

        let mut ctx = Base16Context::new();
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base16Context::decode) method.
    ///
    /// Ciphertext: `B16*66 6f 6F 62`
    ///
    /// Plaintext: `foob`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_11() {
        let ciphertext = vec![
            0x36, 0x36, 0x36, 0x66,
            0x36, 0x46, 0x36, 0x32,
        ];
        let expected_plaintext = vec![0x66, 0x6f, 0x6f, 0x62];

        let mut ctx = Base16Context::new();
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base16Context::decode) method.
    ///
    /// Ciphertext: `B16*66 6f 6f 62 61`
    ///
    /// Plaintext: `fooba`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_12() {
        let ciphertext = vec![
            0x36, 0x36, 0x36, 0x66,
            0x36, 0x66, 0x36, 0x32,
            0x36, 0x31,
        ];
        let expected_plaintext = vec![0x66, 0x6f, 0x6f, 0x62, 0x61];

        let mut ctx = Base16Context::new();
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base16Context::decode) method.
    ///
    /// Ciphertext: `B16*66 6F 6F 62 61 72`
    ///
    /// Plaintext: `foobar`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_13() {
        let ciphertext = vec![
            0x36, 0x36, 0x36, 0x46,
            0x36, 0x46, 0x36, 0x32,
            0x36, 0x31, 0x37, 0x32,
        ];
        let expected_plaintext = vec![0x66, 0x6f, 0x6f, 0x62, 0x61, 0x72];

        let mut ctx = Base16Context::new();
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Additional test covering additional characters of the alphabet for the
    /// [`decode`](Base16Context::decode) method.
    ///
    /// Ciphertext: `B16*E3 81 Bf e3 81 be`
    ///
    /// Plaintext: `みま`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_14() {
        let ciphertext = vec![
            0x45, 0x33, 0x38, 0x31,
            0x42, 0x66, 0x65, 0x33,
            0x38, 0x31, 0x62, 0x65,
        ];
        let expected_plaintext = vec![0xe3, 0x81, 0xbf, 0xe3, 0x81, 0xbe];

        let mut ctx = Base16Context::new();
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`encode`](Base16Context::encode) method.
    ///
    /// Plaintext: `<empty byte array>`
    ///
    /// Ciphertext: `B16*`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_15() {
        let plaintext = vec![];
        let expected_ciphertext = vec![];

        let mut ctx = Base16Context::new();
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Test vector from `RFC 4648` for the [`encode`](Base16Context::encode) method.
    ///
    /// Plaintext: `f`
    ///
    /// Ciphertext: `B16*66`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_16() {
        let plaintext = vec![0x66];
        let expected_ciphertext = vec![
            0x36, 0x36,
        ];

        let mut ctx = Base16Context::new();
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Test vector from `RFC 4648` for the [`encode`](Base16Context::encode) method.
    ///
    /// Plaintext: `fo`
    ///
    /// Ciphertext: `B16*66 6F`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_17() {
        let plaintext = vec![0x66, 0x6f];
        let expected_ciphertext = vec![
            0x36, 0x36, 0x36, 0x46,
        ];

        let mut ctx = Base16Context::new();
        ctx.set_ciphertext_capitalization(true);
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Test vector from `RFC 4648` for the [`encode`](Base16Context::encode) method.
    ///
    /// Plaintext: `foo`
    ///
    /// Ciphertext: `B16*66 6f 6f`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_18() {
        let plaintext = vec![0x66, 0x6f, 0x6f];
        let expected_ciphertext = vec![
            0x36, 0x36, 0x36, 0x66,
            0x36, 0x66,
        ];

        let mut ctx = Base16Context::new();
        ctx.is_capitalized = false;
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Test vector from `RFC 4648` for the [`encode`](Base16Context::encode) method.
    ///
    /// Plaintext: `foob`
    ///
    /// Ciphertext: `B16*66 6F 6F 62`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_19() {
        let plaintext = vec![0x66, 0x6f, 0x6f, 0x62];
        let expected_ciphertext = vec![
            0x36, 0x36, 0x36, 0x46,
            0x36, 0x46, 0x36, 0x32,
        ];

        let mut ctx = Base16Context::new();
        ctx.set_ciphertext_capitalization(true);
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Test vector from `RFC 4648` for the [`encode`](Base16Context::encode) method.
    ///
    /// Plaintext: `fooba`
    ///
    /// Ciphertext: `B16*66 6f 6f 62 61`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_20() {
        let plaintext = vec![0x66, 0x6f, 0x6f, 0x62, 0x61];
        let expected_ciphertext = vec![
            0x36, 0x36, 0x36, 0x66,
            0x36, 0x66, 0x36, 0x32,
            0x36, 0x31,
        ];

        let mut ctx = Base16Context::new();
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Test vector from `RFC 4648` for the [`encode`](Base16Context::encode) method.
    ///
    /// Plaintext: `foobar`
    ///
    /// Ciphertext: `B16*66 6F 6F 62 61 72`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_21() {
        let plaintext = vec![0x66, 0x6f, 0x6f, 0x62, 0x61, 0x72];
        let expected_ciphertext = vec![
            0x36, 0x36, 0x36, 0x46,
            0x36, 0x46, 0x36, 0x32,
            0x36, 0x31, 0x37, 0x32,
        ];

        let mut ctx = Base16Context::new();
        ctx.set_ciphertext_capitalization(true);
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Additional test for the [`encode`](Base16Context::encode) method.
    ///
    /// Plaintext = `みま`
    ///
    /// ```
    /// //                 みま = 0x        3      0    7       f         3      0    7       e
    /// //       (into binary) = 0b        0011   00 0001   11 1111      0011   00'0001   11 1110
    /// //        (into UTF-8) = 0b   1110'0011 1000'0001 1011'1111 1110'0011 1000'0001 1011'1110
    /// //          (into hex) = 0x   e3        81        bf        e3        81        be
    /// //          (Base16()) = B16* e3        81        bf        e3        81        be
    /// ```
    ///
    /// Ciphertext = `B16*e3 81 bf e3 81 be`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_22() {
        let plaintext = vec![0xe3, 0x81, 0xbf, 0xe3, 0x81, 0xbe];
        let expected_ciphertext = vec![
            0x65, 0x33, 0x38, 0x31,
            0x62, 0x66, 0x65, 0x33,
            0x38, 0x31, 0x62, 0x65,
        ];

        let mut ctx = Base16Context::new();
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Tests the [`get_uppercase_decode_alphabet`](Base16Context::get_uppercase_decode_alphabet)
    /// associated function.
    ///
    /// This test is meant as a friendly reminder, if the [`Base16Context`] code will ever be
    /// changed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_23() {
        let mut decode_alphabet: HashMap<u8, u8> = HashMap::new();

        for v in 0..=9 { decode_alphabet.insert(v + 0x30, v); }
        for v in 10..=15 { decode_alphabet.insert(v + 0x37, v); }

        assert_eq![Base16Context::get_uppercase_decode_alphabet().len(), decode_alphabet.len()];
        assert_eq![Base16Context::get_uppercase_decode_alphabet(), decode_alphabet];
    }

    /// Tests the [`get_lowercase_decode_alphabet`](Base16Context::get_lowercase_decode_alphabet)
    /// associated function.
    ///
    /// This test is meant as a friendly reminder, if the [`Base16Context`] code will ever be
    /// changed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_24() {
        let mut decode_alphabet: HashMap<u8, u8> = HashMap::new();

        for v in 0..=9 { decode_alphabet.insert(v + 0x30, v); }
        for v in 10..=15 { decode_alphabet.insert(v + 0x57, v); }

        assert_eq![Base16Context::get_lowercase_decode_alphabet().len(), decode_alphabet.len()];
        assert_eq![Base16Context::get_lowercase_decode_alphabet(), decode_alphabet];
    }

    /// Tests the [`get_uppercase_encode_alphabet`](Base16Context::get_uppercase_encode_alphabet)
    /// associated function.
    ///
    /// This test is meant as a friendly reminder, if the [`Base16Context`] code will ever be
    /// changed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_25() {
        let mut encode_alphabet: [u8; 16] = [0; 16];

        for v in 0x30..=0x39 { encode_alphabet[v - 0x30] = v as u8; }
        for v in 0x41..=0x46 { encode_alphabet[v - 0x37] = v as u8; }

        assert_eq![Base16Context::get_uppercase_encode_alphabet().len(), encode_alphabet.len()];
        assert_eq![Base16Context::get_uppercase_encode_alphabet(), encode_alphabet];
    }

    /// Tests the [`get_lowercase_encode_alphabet`](Base16Context::get_lowercase_encode_alphabet)
    /// associated function.
    ///
    /// This test is meant as a friendly reminder, if the [`Base16Context`] code will ever be
    /// changed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_26() {
        let mut encode_alphabet: [u8; 16] = [0; 16];

        for v in 0x30..=0x39 { encode_alphabet[v - 0x30] = v as u8; }
        for v in 0x61..=0x66 { encode_alphabet[v - 0x57] = v as u8; }

        assert_eq![Base16Context::get_lowercase_encode_alphabet().len(), encode_alphabet.len()];
        assert_eq![Base16Context::get_lowercase_encode_alphabet(), encode_alphabet];
    }

    /// Tests the [`validate_ciphertext`](Base16Context::validate_ciphertext) method.
    ///
    /// Tries validating an empty string ciphertext.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_27() {
        let ctx = Base16Context::new();
        let ciphertext = vec![];
        assert_eq![ctx.validate_ciphertext(&ciphertext), Ok(())];
    }

    /// Tests the [`validate_ciphertext`](Base16Context::validate_ciphertext) method.
    ///
    /// Tries validating a valid uppercase [`Base16Context`] ciphertext.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_28() {
        let mut ctx = Base16Context::new();
        ctx.is_capitalized = true;
        let ciphertext = vec![
            0x30, 0x31, 0x41, 0x42,
            0x43, 0x39, 0x38, 0x37,
            0x36, 0x46, 0x45, 0x44,
            0x32, 0x33, 0x34, 0x35,
        ];
        assert_eq![ctx.validate_ciphertext(&ciphertext), Ok(())];
    }

    /// Tests the [`validate_ciphertext`](Base16Context::validate_ciphertext) method.
    ///
    /// Tries validating a valid lowercase [`Base16Context`] ciphertext.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_29() {
        let mut ctx = Base16Context::new();
        ctx.is_capitalized = false;
        let ciphertext = vec![
            0x30, 0x31, 0x61, 0x62,
            0x63, 0x39, 0x38, 0x37,
            0x36, 0x66, 0x65, 0x64,
            0x32, 0x33, 0x34, 0x35,
        ];
        assert_eq![ctx.validate_ciphertext(&ciphertext), Ok(())];
    }

    /// Tests the [`validate_ciphertext`](Base16Context::validate_ciphertext) method.
    ///
    /// Tries validating an erroneous [`Base16Context`] ciphertext that is in the residue class
    /// `1 + 2ℤ`.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_30() {
        let ctx = Base16Context::new();
        let ciphertext = vec![
            0x32, 0x33, 0x34, 0x35,
            0x36, 0x36, 0x37,
        ];
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::IllegalResidueClass(
            "Failure during Base16 ciphertext validation!".to_string(),
            1, 2,
        ))];
    }

    /// Tests the [`validate_ciphertext`](Base16Context::validate_ciphertext) method.
    ///
    /// Tries validating an erroneous [`Base16Context`] ciphertext that contains a character that is
    /// not part of the [`Base16Context`] alphabet.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_31() {
        let mut ctx = Base16Context::new();
        ctx.is_capitalized = true;
        let ciphertext = vec![
            0x41, 0x42, 0x43, 0x44,
            0xff, 0x46, 0x30, 0x31,
            0x32, 0x33, 0x34, 0x35,
            0x36, 0x37, 0x38, 0x39,
        ];
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::IllegalCharacter(
            "Failure during Base16 ciphertext validation!".to_string(),
            4, ciphertext,
        ))];
    }

    /// Tests the [`validate_ciphertext`](Base16Context::validate_ciphertext) method.
    ///
    /// Tries validating an mixed case but valid [`Base16Context`] ciphertext.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_32() {
        let mut ctx = Base16Context::new();
        ctx.is_capitalized = true;
        let ciphertext = vec![
            0x41, 0x42, 0x43, 0x44,
            0x45, 0x66, 0x30, 0x31,
            0x32, 0x33, 0x34, 0x35,
            0x36, 0x37, 0x38, 0x39,
        ];
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::IllegalCharacter(
            "Failure during Base16 ciphertext validation!".to_string(),
            5, ciphertext,
        ))];
    }
}

// Copyright (C) 2023  Fabian Moos
// This file is part of encodex.
//
// encodex is free software: you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// encodex is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
// even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with encodex. If not,
// see <https://www.gnu.org/licenses/>.
//

//! Functionality for handling [base32hex](Base32HexContext) codes.

use std::collections::hash_map::HashMap;

use crate::{
    AtomClass,
    AtomType,
    CaseInsensitiveCiphertext,
    code::{
        Code,
        Decode,
        Encode,
    },
    CryptoError,
    CryptographicAtom,
    map,
    util::text_to_mono_case,
};

use super::{
    base32_decode,
    base32_encode,
    FOUR_PADDING_BYTE_BIT_STRING,
    ONE_PADDING_BYTE_BIT_STRING,
    SIX_PADDING_BYTE_BIT_STRING,
    THREE_PADDING_BYTE_BIT_STRING,
    ZERO_PADDING_BYTES_BIT_STRING,
};



/// [`CryptographicAtom`] for handling [base32hex](Base32HexContext) plain- and ciphertexts.
///
/// The prefix `B32H*` is used for [base32hex](Base32HexContext) strings in documentation comments.
///
/// For an explanation on how this [`code`](crate::code) works, see
/// `Base32Context`. The only difference between [base32hex](Base32HexContext) and **base32** is the
/// alphabet.
///
/// # Usage Example
///
/// ```
/// use encodex::code::base_encoding::Base32HexContext;
/// use encodex::code::Encode;
/// use encodex::CaseInsensitiveCiphertext;
///
/// let mut ctx = Base32HexContext::new();
/// let ciphertext = ctx.encode(&"Base32HexContext".to_string().into_bytes());
///
/// assert_eq![ciphertext, Ok("89GN6P9J6946AU23DTN78PBOEG======".to_string().into_bytes())];
/// ```
///
/// # Alphabet
///
/// The following table shows the alphabet that is used by the [base32hex](Base32HexContext) code.
/// This alphabet ensures that ciphertexts retain their natural ordering when they are compared
/// bitwise.
///
/// |         || 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | a | b | c | d | e | f |
/// |---------||---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
/// | **0x0** ||`0`|`1`|`2`|`3`|`4`|`5`|`6`|`7`|`8`|`9`|`A`|`B`|`C`|`D`|`E`|`F`|
/// | **0x1** ||`G`|`H`|`I`|`J`|`K`|`L`|`M`|`N`|`O`|`P`|`Q`|`R`|`S`|`T`|`U`|`V`|
/// | **0x2** ||`=`|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
#[derive(Clone, Copy)]
pub struct Base32HexContext {
    /// The [class](AtomClass) this [`CryptographicAtom`] belongs to.
    atom_class: AtomClass,
    /// The [type](AtomType) of every instance of this [`CryptographicAtom`].
    atom_type: AtomType,
    /// The capitalization state of this instance.
    ///
    /// This variable determines if this atom's instance outputs capitalized or small case
    /// ciphertexts.
    ///
    /// The default value is `true`.
    is_capitalized: bool,
}

impl Base32HexContext {
    /// Returns the decode alphabet with only lowercase letters in a [`HashMap`].
    ///
    /// The returned [`HashMap`] maps every ciphertext value to its corresponding `5 bit` index in
    /// the [base32hex](Base32HexContext) alphabet. The padding byte `=` is mapped to `0x20`.
    fn get_lowercase_decode_alphabet() -> HashMap<u8, u64> {
        map![
            (0x30, 0), (0x31, 1), (0x32, 2), (0x33, 3),
            (0x34, 4), (0x35, 5), (0x36, 6), (0x37, 7),
            (0x38, 8), (0x39, 9), (0x61, 10), (0x62, 11),
            (0x63, 12), (0x64, 13), (0x65, 14), (0x66, 15),
            (0x67, 16), (0x68, 17), (0x69, 18), (0x6a, 19),
            (0x6b, 20), (0x6c, 21), (0x6d, 22), (0x6e, 23),
            (0x6f, 24), (0x70, 25), (0x71, 26), (0x72, 27),
            (0x73, 28), (0x74, 29), (0x75, 30), (0x76, 31),
            (0x3d, 32)
        ]
    }

    /// Returns the decode alphabet with only uppercase letters in a [`HashMap`].
    ///
    /// The returned [`HashMap`] maps every ciphertext value to its corresponding `5 bit` index in
    /// the [base32hex](Base32HexContext) alphabet. The padding byte `=` is mapped to `0x20`.
    fn get_uppercase_decode_alphabet() -> HashMap<u8, u64> {
        map![
            (0x30, 0), (0x31, 1), (0x32, 2), (0x33, 3),
            (0x34, 4), (0x35, 5), (0x36, 6), (0x37, 7),
            (0x38, 8), (0x39, 9), (0x41, 10), (0x42, 11),
            (0x43, 12), (0x44, 13), (0x45, 14), (0x46, 15),
            (0x47, 16), (0x48, 17), (0x49, 18), (0x4a, 19),
            (0x4b, 20), (0x4c, 21), (0x4d, 22), (0x4e, 23),
            (0x4f, 24), (0x50, 25), (0x51, 26), (0x52, 27),
            (0x53, 28), (0x54, 29), (0x55, 30), (0x56, 31),
            (0x3d, 32)
        ]
    }

    /// Returns the encode alphabet with only uppercase letters in an array.
    ///
    /// Any `5 bit` block entered into the returned array will immediately return the corresponding
    /// uppercase ciphertext value. For `0x20` the padding byte `=` will be returned.
    fn get_uppercase_encode_alphabet() -> [u8; 33] {
        [
            0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37,
            0x38, 0x39, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46,
            0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e,
            0x4f, 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56,
            0x3d
        ]
    }

    /// Returns the encode alphabet with only lowercase letters in an array.
    ///
    /// Any `5 bit` block entered into the returned array will immediately return the corresponding
    /// lowercase ciphertext value. For `0x20` the padding byte `=` will be returned.
    fn get_lowercase_encode_alphabet() -> [u8; 33] {
        [
            0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37,
            0x38, 0x39, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66,
            0x67, 0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e,
            0x6f, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76,
            0x3d
        ]
    }

    /// Creates a new [`Base32HexContext`] [`CryptographicAtom`].
    pub fn new() -> Base32HexContext {
        Base32HexContext {
            atom_class: AtomClass::Code,
            atom_type: AtomType::Base32Hex,
            is_capitalized: true,
        }
    }

    /// Validates the given ciphertext.
    ///
    /// If the given ciphertext does not match the constraints for [`Base32HexContext`] ciphertexts,
    /// an [error](CryptoError) is returned. The ciphertext is validated for the capitalization mode
    /// that is set on the instance. That means that, before calling this method, the ciphertext
    /// **must** be mono cased.
    fn validate_ciphertext(
        &self,
        ciphertext: &Vec<u8>,
    ) -> Result<(), CryptoError> {
        if ciphertext.is_empty() {
            return Ok(());
        }

        if ciphertext.len() % 8 != 0 {
            return Err(CryptoError::IllegalResidueClass(
                "Failure during Base32hex ciphertext validation!".to_string(),
                (ciphertext.len() % 8) as u8, 8,
            ));
        }

        let alphabet = if self.is_capitalized {
            Base32HexContext::get_uppercase_decode_alphabet()
        } else {
            Base32HexContext::get_lowercase_decode_alphabet()
        };
        let padding_byte = Base32HexContext::get_uppercase_encode_alphabet()[32];
        let mut i = 0;

        for c in &ciphertext[..ciphertext.len() - 6] {
            if !alphabet.contains_key(c) || *c == padding_byte {
                return Err(CryptoError::IllegalCharacter(
                    "Failure during Base32hex ciphertext validation!".to_string(),
                    i, ciphertext.clone(),
                ));
            }
            i += 1;
        }

        let mut last_six_bytes_bit_string = 0;
        for c in &ciphertext[ciphertext.len() - 6..] {
            if !alphabet.contains_key(c) {
                return Err(CryptoError::IllegalCharacter(
                    "Failure during Base32hex ciphertext validation!".to_string(), i,
                    ciphertext.clone(),
                ));
            }
            i += 1;
            if *c == padding_byte {
                last_six_bytes_bit_string |= (*c as u64) << 8 * (ciphertext.len() - i);
            }
        }

        if last_six_bytes_bit_string ^ ZERO_PADDING_BYTES_BIT_STRING == 0x0
            || last_six_bytes_bit_string ^ ONE_PADDING_BYTE_BIT_STRING == 0x0
            || last_six_bytes_bit_string ^ THREE_PADDING_BYTE_BIT_STRING == 0x0
            || last_six_bytes_bit_string ^ FOUR_PADDING_BYTE_BIT_STRING == 0x0
            || last_six_bytes_bit_string ^ SIX_PADDING_BYTE_BIT_STRING == 0x0 {
            Ok(())
        } else {
            Err(CryptoError::MalformedPadding(
                "Failure during Base32hex ciphertext validation!".to_string(),
                ciphertext.clone(),
            ))
        }
    }
}

impl CaseInsensitiveCiphertext for Base32HexContext {
    /// Returns the capitalization state of this [`CryptographicAtom`].
    ///
    /// This method always returns a [`Some`] value for this [`CryptographicAtom`], because
    /// [`Base32HexContext`] can use uppercase and lowercase letters, but, although ciphertexts with
    /// mixed case are allowed, this atom will always produce either uppercase-only or
    /// lowercase-only ciphertexts.
    ///
    /// Additionally, it should be noted, that mixed case ciphertexts miss the purpose of retaining
    /// the natural ordering when compared bitwise, which is the intention of [`Base32HexContext`].
    fn ciphertext_is_capitalized(&self) -> Option<bool> { Some(self.is_capitalized) }

    /// Sets the ciphertext capitalization state of this [`CryptographicAtom`].
    fn set_ciphertext_capitalization(
        &mut self,
        capitalized: bool,
    ) {
        self.is_capitalized = capitalized;
    }
}

impl Code for Base32HexContext {}

impl CryptographicAtom for Base32HexContext {
    /// Returns the [class](AtomClass) the [`Base32HexContext`] belongs to.
    fn get_atom_class(&self) -> AtomClass { self.atom_class }

    /// Returns the [type](AtomType) of this [`CryptographicAtom`].
    fn get_atom_type(&self) -> AtomType { self.atom_type }
}

impl Decode for Base32HexContext {
    /// Decodes the given ciphertext.
    ///
    /// # Errors
    ///
    /// An error is returned when
    ///
    /// * the ciphertext contains bytes that are not part of the [`base32hex`](Base32HexContext)
    ///   alphabet,
    /// * the padding bytes at the end are malformed.
    fn decode(
        &mut self,
        ciphertext: &Vec<u8>,
    ) -> Result<Vec<u8>, CryptoError> {
        let mono_case_text = text_to_mono_case(ciphertext, self.is_capitalized);
        if let Err(error) = self.validate_ciphertext(&mono_case_text) {
            Err(error)
        } else {
            Ok(base32_decode(&mono_case_text, if self.is_capitalized {
                Base32HexContext::get_uppercase_decode_alphabet()
            } else {
                Base32HexContext::get_lowercase_decode_alphabet()
            }))
        }
    }
}

impl Encode for Base32HexContext {
    /// Encodes the given plaintext.
    fn encode(
        &mut self,
        plaintext: &Vec<u8>,
    ) -> Result<Vec<u8>, CryptoError> {
        Ok(base32_encode(plaintext, if self.is_capitalized {
            Base32HexContext::get_uppercase_encode_alphabet()
        } else {
            Base32HexContext::get_lowercase_encode_alphabet()
        }))
    }
}



/// Test vectors for the [base32hex](Base32HexContext) code.
///
/// Some of the tests have been taken from `RFC 4648`. Additional tests have been added to increase
/// data coverage, especially edge cases.
#[cfg(any(feature = "doc_tests", test))]
mod tests {
    use super::*;

    /// Tests the [`get_atom_class`](Base32HexContext::get_atom_class) method of the
    /// [`Base32HexContext`] struct
    ///
    /// This method **must** return [`Code`](AtomClass::Code).
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_01() {
        let ctx = Base32HexContext::new();
        assert_eq![ctx.get_atom_class(), AtomClass::Code];
    }

    /// Tests the [`get_atom_type`](Base32HexContext::get_atom_type) method of the
    /// [`Base32HexContext`] struct.
    ///
    /// This method **must** return [`Base32Hex`](AtomType::Base32Hex).
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_02() {
        let ctx = Base32HexContext::new();
        assert_eq![ctx.get_atom_type(), AtomType::Base32Hex];
    }

    /// Tests the [`ciphertext_is_capitalized`](Base32HexContext::ciphertext_is_capitalized) method.
    ///
    /// The default capitalization after the creation of a new instance of [`Base32HexContext`]
    /// **must** be `true`.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_03() {
        let ctx = Base32HexContext::new();
        assert_eq![ctx.ciphertext_is_capitalized(), Some(true)];
    }

    /// Tests the [`ciphertext_is_capitalized`](Base32HexContext::ciphertext_is_capitalized) method.
    ///
    /// Tests if the capitalization state is `true` after setting it to `true`.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_04() {
        let mut ctx = Base32HexContext::new();
        ctx.set_ciphertext_capitalization(true);
        assert_eq![ctx.ciphertext_is_capitalized(), Some(true)];
    }

    /// Tests the [`ciphertext_is_capitalized`](Base32HexContext::ciphertext_is_capitalized) method.
    ///
    /// Tests if the capitalization state is `true` after setting it to `false` and then to `true`
    /// again.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_05() {
        let mut ctx = Base32HexContext::new();
        ctx.set_ciphertext_capitalization(false);
        ctx.set_ciphertext_capitalization(true);
        assert_eq![ctx.ciphertext_is_capitalized(), Some(true)];
    }

    /// Tests the [`ciphertext_is_capitalized`](Base32HexContext::ciphertext_is_capitalized) method.
    ///
    /// Tests if the capitalization state is `false` after setting it to `false`.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_06() {
        let mut ctx = Base32HexContext::new();
        ctx.set_ciphertext_capitalization(false);
        assert_eq![ctx.ciphertext_is_capitalized(), Some(false)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base32HexContext::decode) method.
    ///
    /// Ciphertext: `B32H*`
    ///
    /// Plaintext: `<empty byte array>`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_07() {
        let ciphertext = vec![];
        let expected_plaintext = vec![];

        let mut ctx = Base32HexContext::new();
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base32HexContext::decode) method.
    ///
    /// Ciphertext: `B32H*CO= === ==`
    ///
    /// Plaintext: `f`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_08() {
        let ciphertext = vec![
            0x43, 0x4f, 0x3d, 0x3d,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];
        let expected_plaintext = vec![0x66];

        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = true;
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base32HexContext::decode) method.
    ///
    /// Ciphertext: `B32H*cpn g== ==`
    ///
    /// Plaintext: `fo`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_09() {
        let ciphertext = vec![
            0x63, 0x70, 0x6e, 0x67,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];
        let expected_plaintext = vec![0x66, 0x6f];

        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = false;
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base32HexContext::decode) method.
    ///
    /// Ciphertext: `B32H*CPN MU= ==`
    ///
    /// Plaintext: `foo`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_10() {
        let ciphertext = vec![
            0x43, 0x50, 0x4e, 0x4d,
            0x55, 0x3d, 0x3d, 0x3d,
        ];
        let expected_plaintext = vec![0x66, 0x6f, 0x6f];

        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = true;
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base32HexContext::decode) method.
    ///
    /// Ciphertext: `B32H*Cpn Muo G=`
    ///
    /// Plaintext: `foob`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_11() {
        let ciphertext = vec![
            0x43, 0x70, 0x6e, 0x4d,
            0x75, 0x6f, 0x47, 0x3d,
        ];
        let expected_plaintext = vec![0x66, 0x6f, 0x6f, 0x62];

        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = true;
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base32HexContext::decode) method.
    ///
    /// Ciphertext: `B32H*cpn muo j1`
    ///
    /// Plaintext: `fooba`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_12() {
        let ciphertext = vec![
            0x63, 0x70, 0x6e, 0x6d,
            0x75, 0x6f, 0x6a, 0x31,
        ];
        let expected_plaintext = vec![0x66, 0x6f, 0x6f, 0x62, 0x61];

        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = true;
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`decode`](Base32HexContext::decode) method.
    ///
    /// Ciphertext: `B32H*cPN MuO J1E 8== === =`
    ///
    /// Plaintext: `foobar`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_13() {
        let ciphertext = vec![
            0x63, 0x50, 0x4e, 0x4d,
            0x75, 0x4f, 0x4a, 0x31,
            0x45, 0x38, 0x3d, 0x3d,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];
        let expected_plaintext = vec![0x66, 0x6f, 0x6f, 0x62, 0x61, 0x72];

        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = false;
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Additional test for the [`decode`](Base32HexContext::decode) method.
    ///
    /// Ciphertext = `B32H*se0 RVO s1n O== === =`
    ///
    /// Plaintext: `みま`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_14() {
        let ciphertext = vec![
            0x73, 0x65, 0x30, 0x52,
            0x56, 0x4f, 0x73, 0x31,
            0x6e, 0x4f, 0x3d, 0x3d,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];
        let expected_plaintext = vec![0xe3, 0x81, 0xbf, 0xe3, 0x81, 0xbe];

        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = false;
        let plaintext = ctx.decode(&ciphertext);

        assert_eq![plaintext, Ok(expected_plaintext)];
    }

    /// Test vector from `RFC 4648` for the [`encode`](Base32HexContext::encode) method.
    ///
    /// Plaintext: `<empty byte array>`
    ///
    /// Ciphertext: `B32H*`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_15() {
        let plaintext = vec![];
        let expected_ciphertext = vec![];

        let mut ctx = Base32HexContext::new();
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Test vector from `RFC 4648` [`encode`](Base32HexContext::encode) method.
    ///
    /// Plaintext: `f`
    ///
    /// Ciphertext: `B32H*CO= === ==`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_16() {
        let plaintext = vec![0x66];
        let expected_ciphertext = vec![
            0x43, 0x4f, 0x3d, 0x3d,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];

        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = true;
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Test vector from `RFC 4648` [`encode`](Base32HexContext::encode) method.
    ///
    /// Plaintext: `fo`
    ///
    /// Ciphertext: `B32*cpn g== ==`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_17() {
        let plaintext = vec![0x66, 0x6f];
        let expected_ciphertext = vec![
            0x63, 0x70, 0x6e, 0x67,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];

        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = false;
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Test vector from `RFC 4648` [`encode`](Base32HexContext::encode) method.
    ///
    /// Plaintext: `foo`
    ///
    /// Ciphertext: `B32H*CPN MU= ==`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_18() {
        let plaintext = vec![0x66, 0x6f, 0x6f];
        let expected_ciphertext = vec![
            0x43, 0x50, 0x4e, 0x4d,
            0x55, 0x3d, 0x3d, 0x3d,
        ];

        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = true;
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Test vector from `RFC 4648` [`encode`](Base32HexContext::encode) method.
    ///
    /// Plaintext: `foob`
    ///
    /// Ciphertext: `B32H*cpn muo g=`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_19() {
        let plaintext = vec![0x66, 0x6f, 0x6f, 0x62];
        let expected_ciphertext = vec![
            0x63, 0x70, 0x6e, 0x6d,
            0x75, 0x6f, 0x67, 0x3d,
        ];

        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = false;
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Test vector from `RFC 4648` [`encode`](Base32HexContext::encode) method.
    ///
    /// Plaintext: `fooba`
    ///
    /// Ciphertext: `B32H*CPN MUO J1`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_20() {
        let plaintext = vec![0x66, 0x6f, 0x6f, 0x62, 0x61];
        let expected_ciphertext = vec![
            0x43, 0x50, 0x4e, 0x4d,
            0x55, 0x4f, 0x4a, 0x31,
        ];

        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = true;
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Test vector from `RFC 4648` [`encode`](Base32HexContext::encode) method.
    ///
    /// Plaintext: `foobar`
    ///
    /// Ciphertext: `B32H*CPN MUO J1E 8== === =`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_21() {
        let plaintext = vec![0x66, 0x6f, 0x6f, 0x62, 0x61, 0x72];
        let expected_ciphertext = vec![
            0x43, 0x50, 0x4e, 0x4d,
            0x55, 0x4f, 0x4a, 0x31,
            0x45, 0x38, 0x3d, 0x3d,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];

        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = true;
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Additional test for the [`encode`](Base32HexContext::encode) method.
    ///
    /// Plaintext = `みま`
    ///
    /// ```
    /// //                 みま = 0x         3      0    7       f         3      0    7       e
    /// //       (into binary) = 0b         0011   00 0001   11 1111      0011   00'0001   11 1110
    /// //        (into UTF-8) = 0b    1110'0011 1000'0001 1011'1111 1110'0011 1000'0001 1011'1110
    /// //          (into hex) = 0x    e3        81        bf        e3        81        be
    /// // (add padding bytes) = 0b    1110 0011 1000 0001 1011 1111 1110 0011 1000 0001 1011 1110
    /// //                             0000 0000 0000 0000 0000 0000 0000 0000
    /// //     (separate into
    /// //       5-bit blocks) = 0b    1'1100 0'1110 0'0000 1'1011 1'1111 1'1000 1'1100 0'0001
    /// //                             1'0111 1'1000'0'0000 0'0000 0'0000 0'0000 0'0000 0'0000
    /// //          (into hex) = 0x    1c     0e     00     1b     1f     18     1c     01
    /// //                             17     18     20     20     20     20     20     20
    /// //          (Base32()) = B32H* S      E      0      R      V      O      S      1
    /// //                             N      O      =      =      =      =      =      =
    /// ```
    ///
    /// Ciphertext = `B32H*se0 rvo s1n o== === =`
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_22() {
        let plaintext = vec![0xe3, 0x81, 0xbf, 0xe3, 0x81, 0xbe];
        let expected_ciphertext = vec![
            0x73, 0x65, 0x30, 0x72,
            0x76, 0x6f, 0x73, 0x31,
            0x6e, 0x6f, 0x3d, 0x3d,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];

        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = false;
        let ciphertext = ctx.encode(&plaintext);

        assert_eq![ciphertext, Ok(expected_ciphertext)];
    }

    /// Tests the [`Base32HexContext`]
    /// [`get_lowercase_decode_alphabet`](Base32HexContext::get_lowercase_decode_alphabet)
    /// associated function.
    ///
    /// This test is meant as a friendly reminder, if the [`Base32HexContext`] code will ever be
    /// changed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_23() {
        let mut decode_alphabet: HashMap<u8, u64> = HashMap::new();

        let mut k = 0x2f;
        for v in 0..=9 {
            k += 1;
            decode_alphabet.insert(k, v);
        }

        k = 0x60;
        for v in 10..=31 {
            k += 1;
            decode_alphabet.insert(k, v);
        }

        decode_alphabet.insert(0x3d, 32);

        assert_eq![Base32HexContext::get_lowercase_decode_alphabet().len(), decode_alphabet.len()];
        assert_eq![Base32HexContext::get_lowercase_decode_alphabet(), decode_alphabet];
    }

    /// Tests the [`Base32HexContext`]
    /// [`get_uppercase_decode_alphabet`](Base32HexContext::get_uppercase_decode_alphabet)
    /// associated function.
    ///
    /// This test is meant as a friendly reminder, if the [`Base32HexContext`] code will ever be
    /// changed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_24() {
        let mut decode_alphabet: HashMap<u8, u64> = HashMap::new();

        let mut k = 0x2f;
        for v in 0..=9 {
            k += 1;
            decode_alphabet.insert(k, v);
        }

        k = 0x40;
        for v in 10..=31 {
            k += 1;
            decode_alphabet.insert(k, v);
        }

        decode_alphabet.insert(0x3d, 32);

        assert_eq![Base32HexContext::get_uppercase_decode_alphabet().len(), decode_alphabet.len()];
        assert_eq![Base32HexContext::get_uppercase_decode_alphabet(), decode_alphabet];
    }

    /// Tests the [`Base32HexContext`]
    /// [`get_uppercase_encode_alphabet`](Base32HexContext::get_uppercase_encode_alphabet)
    /// associated function.
    ///
    /// This test is meant as a friendly reminder, if the [`Base32HexContext`] code will ever be
    /// changed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_25() {
        let mut encode_alphabet: [u8; 33] = [0; 33];

        let mut k = 0;
        for v in 0x30..=0x39 {
            encode_alphabet[k] = v;
            k += 1;
        }
        for v in 0x41..=0x56 {
            encode_alphabet[k] = v;
            k += 1;
        }
        encode_alphabet[k] = 0x3d;

        assert_eq![Base32HexContext::get_uppercase_encode_alphabet().len(), encode_alphabet.len()];
        assert_eq![Base32HexContext::get_uppercase_encode_alphabet(), encode_alphabet];
    }

    /// Tests the [`Base32HexContext`]
    /// [`get_lowercase_encode_alphabet`](Base32HexContext::get_lowercase_encode_alphabet)
    /// associated function.
    ///
    /// This test is meant as a friendly reminder, if the [`Base32HexContext`] code will ever be
    /// changed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_26() {
        let mut encode_alphabet: [u8; 33] = [0; 33];

        let mut k = 0;
        for v in 0x30..=0x39 {
            encode_alphabet[k] = v;
            k += 1;
        }
        for v in 0x61..=0x76 {
            encode_alphabet[k] = v;
            k += 1;
        }
        encode_alphabet[k] = 0x3d;

        assert_eq![Base32HexContext::get_lowercase_encode_alphabet().len(), encode_alphabet.len()];
        assert_eq![Base32HexContext::get_lowercase_encode_alphabet(), encode_alphabet];
    }

    /// Tests the [`validate_ciphertext`](Base32HexContext::validate_ciphertext) method.
    ///
    /// Validating an empty string ciphertext **must** succeed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_27() {
        let ciphertext = vec![];
        let ctx = Base32HexContext::new();
        assert_eq![ctx.validate_ciphertext(&ciphertext), Ok(())];
    }

    /// Tests the [`validate_ciphertext`](Base32HexContext::validate_ciphertext) method.
    ///
    /// Validating a [`Base32HexContext`] ciphertext that contains no padding bytes `=` **must**
    /// succeed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_28() {
        let ciphertext = vec![
            0x30, 0x31, 0x32, 0x33,
            0x34, 0x35, 0x36, 0x37,
        ];
        let ctx = Base32HexContext::new();
        assert_eq![ctx.validate_ciphertext(&ciphertext), Ok(())];
    }

    /// Tests the [`validate_ciphertext`](Base32HexContext::validate_ciphertext) method.
    ///
    /// Validating a [`Base32HexContext`] ciphertext that contains exactly one padding byte `=`
    /// **must** succeed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_29() {
        let ciphertext = vec![
            0x38, 0x39, 0x61, 0x62,
            0x63, 0x64, 0x65, 0x3d,
        ];
        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = false;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Ok(())];
    }

    /// Tests the [`validate_ciphertext`](Base32HexContext::validate_ciphertext) method.
    ///
    /// Validating a [`Base32HexContext`] ciphertext with exactly three padding bytes `=` **must**
    /// succeed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_30() {
        let ciphertext = vec![
            0x47, 0x48, 0x49, 0x4a,
            0x4b, 0x3d, 0x3d, 0x3d,
        ];
        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = true;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Ok(())];
    }

    /// Tests the [`validate_ciphertext`](Base32HexContext::validate_ciphertext) method.
    ///
    /// Validating a valid [`Base32HexContext`] ciphertext with exactly four padding bytes `=`
    /// **must** succeed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_31() {
        let ciphertext = vec![
            0x6f, 0x70, 0x71, 0x72,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];
        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = false;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Ok(())];
    }

    /// Tests the [`validate_ciphertext`](Base32HexContext::validate_ciphertext) method.
    ///
    /// Validating a [`Base32HexContext`] ciphertext with exactly six padding bytes `=` **must**
    /// succeed.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_32() {
        let ciphertext = vec![
            0x53, 0x54, 0x3d, 0x3d,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];
        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = true;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Ok(())];
    }

    /// Tests the [`validate_ciphertext`](Base32HexContext::validate_ciphertext) method.
    ///
    /// Validating a [`Base32HexContext`] ciphertext that has a wrong residue class **must not**
    /// succeed.
    ///
    /// Only one test is required because all wrong residue class errors are handled by the same
    /// branch in the [`validate_ciphertext`](Base32HexContext::validate_ciphertext) method.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_33() {
        let ciphertext = vec![
            0x30, 0x31, 0x32, 0x33,
        ];
        let ctx = Base32HexContext::new();
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::IllegalResidueClass(
            "Failure during Base32hex ciphertext validation!".to_string(), 4, 8,
        ))];
    }

    /// Tests the [`validate_ciphertext`](Base32HexContext::validate_ciphertext) method.
    ///
    /// Validating a [`Base32HexContext`] ciphertext that contains a character that is not part of
    /// the [`Base32HexContext`] alphabet and is not one of the last six bytes of the ciphertext
    /// **must** fail.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_34() {
        let ciphertext = vec![
            0x34, 0x35, 0x36, 0x37,
            0xff, 0x38, 0x39, 0x41,
            0x42, 0x43, 0x44, 0x45,
            0x46, 0x47, 0x48, 0x49,
        ];
        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = false;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::IllegalCharacter(
            "Failure during Base32hex ciphertext validation!".to_string(), 4,
            ciphertext,
        ))];
    }

    /// Tests the [`validate_ciphertext`](Base32HexContext::validate_ciphertext) method.
    ///
    /// Validating a [`Base32HexContext`] ciphertext that contains a padding byte that appears
    /// before the sixth last position of the ciphertext **must** fail.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_35() {
        let ciphertext = vec![
            0x4a, 0x50, 0x51, 0x52,
            0x53, 0x54, 0x55, 0x56,
            0x3d, 0x3d, 0x3d, 0x3d,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];
        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = true;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::IllegalCharacter(
            "Failure during Base32hex ciphertext validation!".to_string(), 8,
            ciphertext,
        ))];
    }

    /// Tests the [`validate_ciphertext`](Base32HexContext::validate_ciphertext) method.
    ///
    /// Validating a [`Base32HexContext`] ciphertext that contains a character that is not part of
    /// the [`Base32HexContext`] alphabet and is among the last six bytes of the ciphertext **must**
    /// fail.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_36() {
        let mut ciphertext = vec![
            0x30, 0x31, 0x32, 0x33,
            0x34, 0x35, 0x36, 0x00,
        ];
        let ctx = Base32HexContext::new();
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::IllegalCharacter(
            "Failure during Base32hex ciphertext validation!".to_string(),
            7, ciphertext.clone(),
        ))];
        let mut c = 0x38;
        for i in (3..=7).rev() {
            c -= 1;
            ciphertext[i] = c;
            ciphertext[i - 1] = 0x00;
            assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::IllegalCharacter(
                "Failure during Base32hex ciphertext validation!".to_string(), i - 1,
                ciphertext.clone(),
            ))];
        }
    }

    /// Tests the [`validate_ciphertext`](Base32HexContext::validate_ciphertext) method.
    ///
    /// Validating a [`Base32HexContext`] ciphertext with malformed padding bytes **must** fail.
    /// This test uses a [`Base32HexContext`] ciphertext with exactly five padding bytes `=` on the
    /// last five positions of the ciphertext.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_37() {
        let ciphertext = vec![
            0x38, 0x39, 0x61, 0x3d,
            0x3d, 0x3d, 0x3d, 0x3d,
        ];
        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = false;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::MalformedPadding(
            "Failure during Base32hex ciphertext validation!".to_string(),
            ciphertext,
        ))];
    }

    /// Tests the [`validate_ciphertext`](Base32HexContext::validate_ciphertext) method.
    ///
    /// Validating a [`Base32HexContext`] ciphertext with malformed padding bytes **must** fail.
    /// This test uses a [`Base32HexContext`] ciphertext with exactly two padding bytes `=` on the
    /// last two positions of the ciphertext.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_38() {
        let ciphertext = vec![
            0x43, 0x44, 0x45, 0x46,
            0x47, 0x48, 0x3d, 0x3d,
        ];
        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = true;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::MalformedPadding(
            "Failure during Base32hex ciphertext validation!".to_string(),
            ciphertext,
        ))];
    }

    /// Tests the [`validate_ciphertext`](Base32HexContext::validate_ciphertext) method.
    ///
    /// Validating a [`Base32HexContext`] ciphertext with malformed padding bytes **must** fail.
    /// This test uses a [`Base32HexContext`] ciphertext with one padding byte on the sixth last and
    /// one padding byte on the last position of the ciphertext.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_39() {
        let ciphertext = vec![
            0x6b, 0x6c, 0x3d, 0x6e,
            0x6f, 0x70, 0x71, 0x3d,
        ];
        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = false;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::MalformedPadding(
            "Failure during Base32hex ciphertext validation!".to_string(),
            ciphertext,
        ))];
    }

    /// Tests the [`validate_ciphertext`](Base32HexContext::validate_ciphertext) method.
    ///
    /// Validating a [`Base32HexContext`] ciphertext with malformed padding bytes **must** fail.
    /// This test uses a [`Base32HexContext`] ciphertext with two padding bytes on the sixth last
    /// and fifth last position and one padding byte on the second last position of the ciphertext.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_40() {
        let ciphertext = vec![
            0x53, 0x54, 0x3d, 0x3d,
            0x30, 0x31, 0x3d, 0x33,
        ];
        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = true;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::MalformedPadding(
            "Failure during Base32hex ciphertext validation!".to_string(),
            ciphertext,
        ))];
    }

    /// Tests the [`validate_ciphertext`](Base32HexContext::validate_ciphertext) method.
    ///
    /// Validating a [`Base32HexContext`] ciphertext with malformed padding bytes **must** fail.
    /// This test uses a [`Base32HexContext`] ciphertext with five padding bytes on the sixth last
    /// to the second last position of the ciphertext.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_41() {
        let ciphertext = vec![
            0x34, 0x35, 0x3d, 0x3d,
            0x3d, 0x3d, 0x3d, 0x62,
        ];
        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = false;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::MalformedPadding(
            "Failure during Base32hex ciphertext validation!".to_string(),
            ciphertext,
        ))];
    }

    /// Tests the [`validate_ciphertext`](Base32HexContext::validate_ciphertext) method.
    ///
    /// Validating a [`Base32HexContext`] ciphertext with malformed padding bytes **must** fail.
    /// This test uses a [`Base32HexContext`] ciphertext with one padding byte on the third last
    /// position of the ciphertext.
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_42() {
        let ciphertext = vec![
            0x43, 0x44, 0x45, 0x46,
            0x47, 0x3d, 0x48, 0x49,
        ];
        let mut ctx = Base32HexContext::new();
        ctx.is_capitalized = true;
        assert_eq![ctx.validate_ciphertext(&ciphertext), Err(CryptoError::MalformedPadding(
            "Failure during Base32hex ciphertext validation!".to_string(),
            ciphertext,
        ))];
    }
}

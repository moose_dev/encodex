// Copyright (C) 2023  Fabian Moos
// This file is part of encodex.
//
// encodex is free software: you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// encodex is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
// even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with encodex. If not,
// see <https://www.gnu.org/licenses/>.
//

//! Functions that are used in several modules in this crate.

/// Changes all letters in a text to upper- or lowercase depending on its arguments.
///
/// Every byte that is already lowercase or is not even a letter is just added to the new text
/// unchanged.
#[cfg(any(
feature = "shift_cipher",
feature = "base16",
feature = "base32",
feature = "base32hex",
feature = "hex"
))]
pub(crate) fn text_to_mono_case(
    text: &Vec<u8>,
    to_uppercase: bool,
) -> Vec<u8> {
    let mut mono_case_text = Vec::new();

    for l in text {
        if to_uppercase && 0x61 <= *l && *l <= 0x7a {
            mono_case_text.push(*l - 0x20);
        } else if !to_uppercase && 0x41 <= *l && *l <= 0x5a {
            mono_case_text.push(*l + 0x20);
        } else {
            mono_case_text.push(*l);
        }
    }

    mono_case_text
}



/// Tests for functions that are accessible from anywhere in the crate.
#[cfg(any(feature = "doc_tests", test))]
mod tests {
    #[cfg(any(
    feature = "shift_cipher",
    feature = "base16",
    feature = "base32",
    feature = "base32hex",
    feature = "hex"
    ))]
    use super::*;

    /// Tests the [`text_to_mono_case`] function.
    ///
    /// Tests if the uppercase text `VIGENERE` will be changed to `vigenere`.
    #[cfg(any(
    feature = "shift_cipher",
    feature = "base16",
    feature = "base32",
    feature = "base32hex",
    feature = "hex"
    ))]
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_01() {
        let text = vec![0x56, 0x49, 0x47, 0x45, 0x4e, 0x45, 0x52, 0x45];
        let expected_text = vec![0x76, 0x69, 0x67, 0x65, 0x6e, 0x65, 0x72, 0x65];
        let calculated_text = text_to_mono_case(&text, false);
        assert_eq![calculated_text, expected_text];
    }

    /// Tests the [`text_to_mono_case`] function.
    ///
    /// Tests if the lowercase text `vigenere` will be changed to `VIGENERE`.
    #[cfg(any(
    feature = "shift_cipher",
    feature = "base16",
    feature = "base32",
    feature = "base32hex",
    feature = "hex"
    ))]
    #[cfg_attr(not(feature = "doc_tests"), test)]
    fn test_02() {
        let text = vec![0x76, 0x69, 0x67, 0x65, 0x6e, 0x65, 0x72, 0x65];
        let expected_text = vec![0x56, 0x49, 0x47, 0x45, 0x4e, 0x45, 0x52, 0x45];
        let calculated_text = text_to_mono_case(&text, true);
        assert_eq![calculated_text, expected_text];
    }
}

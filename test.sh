########## def:features ############################################################################
CAESAR_FEATURE="caesar"
VIGENERE_FEATURE="vigenere"

BASE16_FEATURE="base16"
BASE32_FEATURE="base32"
BASE32HEX_FEATURE="base32hex"
BASE64_FEATURE="base64"
BASE64URL_FEATURE="base64url"
HEX_FEATURE="hex"

SHA256_FEATURE="sha256"

########## def:cipher ##############################################################################

SHIFT_CIPHER_FEATURES="$CAESAR_FEATURE,$VIGENERE_FEATURE"

CIPHER_FEATURES="$SHIFT_CIPHER_FEATURES"

########## def:code ################################################################################

BASE_ENCODING_FEATURES="$BASE16_FEATURE,$HEX_FEATURE,$BASE32_FEATURE,$BASE32HEX_FEATURE,"
BASE_ENCODING_FEATURES="$BASE_ENCODING_FEATURES,$BASE64_FEATURE,$BASE64URL_FEATURE"

CODE_FEATURES="$BASE_ENCODING_FEATURES"

########## def:digest ##############################################################################

SHA_2_FEATURES="$SHA256_FEATURE"

DIGEST_FEATURES="$SHA_2_FEATURES"

########## def:all #################################################################################

ALL_FEATURES="$CIPHER_FEATURES,$CODE_FEATURES,$DIGEST_FEATURES"



####################################################################################################
########## CIPHER ##################################################################################
####################################################################################################

# Test all shift cipher features individually
echo ""
echo ""
echo ""
echo "Now running: \"cargo test --lib --features $VIGENERE_FEATURE \""
cargo test --lib --features $VIGENERE_FEATURE

#Test all shift cipher features together
echo ""
echo ""
echo ""
echo "Now running: \"cargo test --lib --features $SHIFT_CIPHER_FEATURES\""
cargo test --lib --features $SHIFT_CIPHER_FEATURES

# Test all ciphers together
echo ""
echo ""
echo ""
echo "Now running: \"cargo test --lib --features $CIPHER_FEATURES\""
cargo test --lib --features $CIPHER_FEATURES

####################################################################################################
########## CODE ####################################################################################
####################################################################################################

# Test all base encoding features individually
echo ""
echo ""
echo ""
echo "Now running: \"cargo test --lib --features $BASE16_FEATURE\""
cargo test --lib --features $BASE16_FEATURE
echo ""
echo ""
echo ""
echo "Now running: \"cargo test --lib --features $BASE32_FEATURE\""
cargo test --lib --features $BASE32_FEATURE
echo ""
echo ""
echo ""
echo "Now running: \"cargo test --lib --features $BASE32HEX_FEATURE\""
cargo test --lib --features $BASE32HEX_FEATURE
echo ""
echo ""
echo ""
echo "Now running: \"cargo test --lib --features $BASE64_FEATURE\""
cargo test --lib --features $BASE64_FEATURE
echo ""
echo ""
echo ""
echo "Now running: \"cargo test --lib --features $BASE64URL_FEATURE\""
cargo test --lib --features $BASE64URL_FEATURE

# Test all base encoding features together
echo ""
echo ""
echo ""
echo "Now running: \"cargo test --lib --features $BASE_ENCODING_FEATURES\""
cargo test --lib --features $BASE_ENCODING_FEATURES

# Test all code features together
echo ""
echo ""
echo ""
echo "Now running: \"cargo test --lib --features $CODE_FEATURES\""
cargo test --lib --features $CODE_FEATURES

####################################################################################################
########## DIGEST ##################################################################################
####################################################################################################

# Test all sha-2 features individually
echo ""
echo ""
echo ""
echo "Now running: \"cargo test --lib --features $SHA256_FEATURE\""
cargo test --lib --features $SHA256_FEATURE

# Test all sha-2 features together
echo ""
echo ""
echo ""
echo "Now running: \"cargo test --lib --features $SHA_2_FEATURES\""
cargo test --lib --features $SHA_2_FEATURES

# Test all digest features together
echo ""
echo ""
echo ""
echo "Now running: \"cargo test --lib --features $DIGEST_FEATURES\""
cargo test --lib --features $DIGEST_FEATURES

####################################################################################################
########## ALL FEATURES ############################################################################
####################################################################################################

echo ""
echo ""
echo ""
echo "Now running: \"cargo test --lib --features $ALL_FEATURES\""
cargo test --lib --features $ALL_FEATURES
